package com.us.masterproject.service;

import com.us.masterproject.dto.UserResponse;
import com.us.masterproject.exception.InvalidDataException;
import com.us.masterproject.exception.NotFoundException;

public interface UserService {
    UserResponse loginUser(Integer roleId, String userName, String password, String logInTime) throws NotFoundException, InvalidDataException;

    String updatePassword(Integer id, Integer roleId, String password) throws NotFoundException, InvalidDataException;
}
