package com.us.masterproject.service;

import com.us.masterproject.dto.*;
import com.us.masterproject.exception.InvalidDataException;
import com.us.masterproject.exception.NotFoundException;
import com.us.masterproject.model.Clients;
import com.us.masterproject.model.ConvertedClient;
import com.us.masterproject.model.Installment;

import java.text.ParseException;
import java.util.Date;
import java.util.List;


public interface ClientService {

    public Clients saveOrUpdateClient(AddClientReq addClientReq) throws NotFoundException, ParseException, InvalidDataException;

    public List<ClientsResponse> getAllClientsResponseList();

    void deleteFreshClientById(SoftDeleteDto freshClientId) throws InvalidDataException;

    ConvertedClient saveConvertedClient(ConvertedClientReq convertedClientReq) throws NotFoundException, ParseException, InvalidDataException;

    List<ClientsResponse> getAllPendingClientByDate(Integer executiveId, Date followUpDate) throws ParseException, NotFoundException;

    List<ConvertedClientResponse> getAllConvertedClientByDate(Integer executiveId, Date followUpDate) throws NotFoundException;

    List<ConvertedClientResponse> getAllConvertedClientByExecutiveId(Integer executiveId) throws NotFoundException;

    List<ClientsResponse> getAllPendingClientByExecutiveId(Integer executiveId) throws NotFoundException;

    ClientsResponse getPendingClientById(Integer pendingClientId) throws NotFoundException;

    ConvertedClientResponse getConvertedClientById(Integer convertedClientId) throws NotFoundException;

    Installment updateInstallment(InstallmentReq addClientReq) throws NotFoundException, InvalidDataException;

    TargetResponse getTargetByDate(Integer executiveId, String targetDate) throws NotFoundException;

    Integer getConvertedClientCountByExecutiveId(Integer executiveId, Integer month, Integer year);

    void changeCompletedStatusByConvertedClientId(Integer convertedClientId, Boolean status) throws NotFoundException;
}
