package com.us.masterproject.service;

import com.us.masterproject.dto.AddAdminReq;
import com.us.masterproject.dto.AdminResponse;
import com.us.masterproject.exception.InvalidDataException;
import com.us.masterproject.exception.NotFoundException;
import com.us.masterproject.model.Admin;

import java.util.List;

public interface SuperAdminService {

    Admin addUpdateAdmin(AddAdminReq addAdminReq) throws NotFoundException, InvalidDataException;

    AdminResponse getAdminById(Integer adminId) throws NotFoundException;

    List<AdminResponse> getAllAdmin();

    void deleteAdminById(Integer adminId) throws NotFoundException;
}
