package com.us.masterproject.service;

import com.us.masterproject.dto.AddEodFormReq;
import com.us.masterproject.dto.AddFreshClient;
import com.us.masterproject.dto.EodFormResponse;
import com.us.masterproject.dto.FreshClientResponse;
import com.us.masterproject.exception.InvalidDataException;
import com.us.masterproject.exception.NotFoundException;
import com.us.masterproject.model.EodForm;
import com.us.masterproject.model.FreshClient;

import java.util.Date;
import java.util.List;

public interface ExecutiveService {
    List<FreshClient> getAllFreshClientResponseList();

    FreshClientResponse getFreshClientById(Integer freshClientId) throws NotFoundException;

    List<FreshClientResponse> getAllFreshClientByExecutiveId(Integer executiveId) throws InvalidDataException;

    EodForm addUpdateEodForm(AddEodFormReq addEodFormReq) throws NotFoundException, InvalidDataException;

    List<EodFormResponse> getAllEodFormDetailsByExecutiveId(Integer executiveId) throws NotFoundException, InvalidDataException;

    Boolean getEodFormDetailsByDate(Integer executiveId, Date todayDate);

    FreshClient addUpdateFreshClient(AddFreshClient addFreshClient) throws NotFoundException, InvalidDataException;
}
