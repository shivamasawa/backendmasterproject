package com.us.masterproject.service;

import com.us.masterproject.dto.*;
import com.us.masterproject.exception.InvalidDataException;
import com.us.masterproject.exception.NotFoundException;
import com.us.masterproject.model.ExcelSheetUrl;
import com.us.masterproject.model.Executive;
import com.us.masterproject.model.Target;

import java.io.InputStream;
import java.util.List;

public interface AdminService {

    String uploadFileFromExcel(InputStream in, Integer executiveId) throws InvalidDataException, NotFoundException;

    Executive addUpdateExecutive(AddExecutiveReq addExecutiveReq) throws NotFoundException, InvalidDataException;

    List<Executive> getAllExecutive();

    ExecutiveResponse getExecutiveById(Integer executiveId) throws NotFoundException;

    List<ExecutiveResponse> getAllExecutiveByAdminId(Integer adminId) throws NotFoundException;

    void deleteExecutiveById(Integer executiveId) throws NotFoundException;

    Target addUpdateTarget(AddTargetReq addTargetReq) throws NotFoundException, InvalidDataException;

    List<TargetResponse> getAllTargetByExecutiveId(Integer executiveId) throws NotFoundException;

    List<AttendanceResponse> getAllAttendanceByExecutiveId(Integer executiveId, Integer month, Integer year) throws NotFoundException, InvalidDataException;

    TargetResponse getTargetByTargetId(Integer targetId) throws NotFoundException;

    ExcelSheetUrl addUpdateExcelSheetUrl(ExcelSheetUrlDto excelSheetUrlDto) throws NotFoundException, InvalidDataException;

    List<ExcelSheetUrlDto> getAllExcelSheetUrlByAdminId(Integer adminId) throws NotFoundException, InvalidDataException;

    void deleteExcelSheetUrlById(Integer excelSheetUrlId) throws NotFoundException;
}
