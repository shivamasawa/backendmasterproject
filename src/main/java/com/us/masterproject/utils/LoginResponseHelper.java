package com.us.masterproject.utils;

import com.us.masterproject.dto.UserResponse;
import com.us.masterproject.model.Admin;
import com.us.masterproject.model.ExcelSheetUrl;
import com.us.masterproject.model.Executive;
import com.us.masterproject.repository.ExcelSheetUrlRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Collections.emptyList;

@Component
public class LoginResponseHelper {

    @Autowired
    private ExcelSheetUrlRepository excelSheetUrlRepository;

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public UserResponse buildSuperAdminResponse(Admin superAdmin) {
        return UserResponse.builder()
                .id(superAdmin.getId())
                .userName(superAdmin.getUserName())
                .email(superAdmin.getEmailId())
                .createdBy(superAdmin.getCreatedBy())
                .role(superAdmin.getRole())
                .build();
    }

    public UserResponse buildAdminResponse(Admin admin) {
        List<ExcelSheetUrl> getExcelSheetUrl = excelSheetUrlRepository.findByAdminId(admin.getId());
        return UserResponse.builder()
                .id(admin.getId())
                .userName(admin.getUserName())
                .email(admin.getEmailId())
                .createdBy(admin.getCreatedBy())
                .role(admin.getRole())
                .companyLogoName(admin.getCompanyLogoUrl())
                .excelSheetUrl(null)
                .build();
    }

    private List<String> getExcelSheetUrl(List<ExcelSheetUrl> getExcelSheetUrl) {
        if (!CollectionUtils.isEmpty(getExcelSheetUrl))
            return getExcelSheetUrl.stream().map(this::excelSheetTranslate).collect(Collectors.toList());
        else
            return emptyList();
    }

    private String excelSheetTranslate(ExcelSheetUrl excelSheetUrl) {
        ExcelSheetUrl getExcelSheetUrl = new ExcelSheetUrl();
        return getExcelSheetUrl.getExcelSheetUrl();
    }


    public UserResponse buildExecutiveResponse(Executive executive) {
        return UserResponse.builder()
                .id(executive.getId())
                .userName(executive.getUserName())
                .email(executive.getEmailId())
                .createdBy(executive.getCreatedBy())
                .role(executive.getRole())
                .companyLogoName(executive.getAdmin() != null && executive.getAdmin().getCompanyLogoUrl() != null ? executive.getAdmin().getCompanyLogoUrl() : null)
                .build();
    }

}
