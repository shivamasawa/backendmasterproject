package com.us.masterproject.utils;

public class ModelConstant {

    public static final String SUCCESS = "success" ;

    public static final String PASSWORD_MUST = "password must be required" ;

    public static final String ROLE_ID_MUST = "role id must be required" ;

    public static final String ID_MUST = "id must be required" ;

    public static final String ADMIN_ID_MUST = "admin id must be required";

    public static final String ADD_EXECUTIVE_REQ_MUST = "executive request dto should not be empty" ;

    public static final String ADD_ADMIN_REQ_MUST = "admin request dto should not be empty";

    public static final String ADD_CLIENT_REQ_MUST = "client request dto should not be empty";

    public static final String EOD_REQ_MUST = "eod form request dto should not be empty";

    public static final String EXECUTIVE_ID_MUST = "executive id must be required";

    public static final String INSTALLMENT_ID_MUST = "installment id must be required";

    public static final String ADD_CONVERTED_CLIENT_REQ_MUST = "converted client request dto should not be empty";

    public static final String PENDING = "pending";

    public static final String CONVERTED = "converted";

    public static final String FRESH_CLIENT = "freshClient";

    public static final String FRESH_CLIENT_ID_MUST = "fresh client id must be required";

    public static final String PENDING_CLIENT_ID_MUST = "pending client id must be required";

    public static final String EMAIL_EXIST = "This Email Already Exist";

    public static final String CONTACT_NO_EXIST = "This Contact Number Already Exist";

    public static final String USER_NAME_EXIST =  "This User Name Already Exist";

    public static final String EOD_ALREADY_EXIST =  "EOD Detail Already Exist With This Executive";

    public static final String EOD_DATE_MUST =  "eOD date must be required";

    public static final String TARGET_ALREADY_EXIST =  "TARGET Detail Already Exist With This Executive";

    public static final String FRESH_CLIENT_REQ_MUST = "fresh client request dto should not be empty";

    public static final String FILE_MUST = "Excel file must be required";

    public static final String EXCEL_SHEET_URL_MUST = "ExcelSheet url must be required";

    public static final String EXCEL_SHEET_REQ_MUST = "ExcelSheet request must be required";

}
