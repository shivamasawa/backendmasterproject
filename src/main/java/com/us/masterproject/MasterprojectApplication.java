package com.us.masterproject;

import com.us.masterproject.utils.FileStorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableScheduling
@EnableWebMvc
@EnableSwagger2
@EnableConfigurationProperties({
		FileStorageProperties.class
})
public class MasterprojectApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(MasterprojectApplication.class, args);
	}

}
