package com.us.masterproject.repository;

import com.us.masterproject.model.EodForm;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface EodFormRepository extends JpaRepository<EodForm,Integer> {

    List<EodForm> findByExecutiveId(Integer executiveId);

    Boolean existsByExecutiveIdAndCreatedDate(Integer executiveId, Date followDate);
}
