package com.us.masterproject.repository;

import com.us.masterproject.model.Installment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InstallmentRepository extends JpaRepository<Installment,Integer> {

    List<Installment> findByConvertedClientId(Integer id);
}
