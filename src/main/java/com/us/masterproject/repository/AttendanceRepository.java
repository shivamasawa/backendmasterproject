package com.us.masterproject.repository;

import com.us.masterproject.model.Attendance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface AttendanceRepository extends JpaRepository<Attendance,Integer> {
    Attendance findByExecutiveIdAndCreatedDate(Integer id, Date date);

    @Query(value = "select * from attendance a where month(a.created_date) =:month and year(a.created_date) =:year and executive_id =:executiveId",nativeQuery = true)
    List<Attendance> getAllAttendanceByExecutiveId(Integer executiveId, Integer month, Integer year);
}
