package com.us.masterproject.repository;

import com.us.masterproject.model.Target;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TargetRepository extends JpaRepository<Target,Integer> {

    List<Target> findAllByExecutiveId(Integer executiveId);

    Target findByExecutiveIdAndTargetDate(Integer executiveId, String targetDate);

    Boolean existsByExecutiveIdAndTargetDate(Integer executiveId, String targetDate);
}
