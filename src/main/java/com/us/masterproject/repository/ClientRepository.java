package com.us.masterproject.repository;

import com.us.masterproject.model.Clients;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface ClientRepository extends JpaRepository<Clients, Integer> {

    public List<Clients> findByNextFollowupDateAndStatus(Date followUpDate, String string);

    List<Clients> findByExecutiveIdAndStatusAndIsActive(Integer executiveId, String status, boolean isActive);

    @Query(value = "SELECT * FROM client WHERE executive_id =:executiveId AND next_followup_date =:followUpDate AND status =:status AND is_active =true", nativeQuery = true)
    List<Clients> findByExecutiveIdAndNextFollowupDateAndStatusAndIsActive(Integer executiveId, String followUpDate, String status);

    Clients findByIdAndIsActive(Integer pendingClientId, boolean isActive);

    @Query(value = "SELECT * FROM client WHERE next_followup_date =:followUpDate AND status =:status AND is_active =true", nativeQuery = true)
    List<Clients> findByNextFollowupDateAndStatusAndIsActive(String followUpDate, String status);
}
