package com.us.masterproject.repository;

import com.us.masterproject.model.FreshClient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FreshClientRepository extends JpaRepository<FreshClient,Integer> {
    @Query(value = "SELECT * FROM fresh_client WHERE executive_id =:executiveId AND is_active= true AND status=:status",nativeQuery = true)
    List<FreshClient> getAllFreshClientByExecutiveId(Integer executiveId, String status);

    boolean existsByContactNumber(String contactNumber);
}
