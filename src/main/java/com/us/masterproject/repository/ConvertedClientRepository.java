package com.us.masterproject.repository;

import com.us.masterproject.model.ConvertedClient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConvertedClientRepository extends JpaRepository<ConvertedClient,Integer> {

    @Query(value = "SELECT * FROM converted_client WHERE executive_id =:executiveId AND next_followup_date =:followUpDate AND status =:status AND is_active = true AND is_completed = false",nativeQuery = true)
    List<ConvertedClient> getAllConvertedClientByDate(Integer executiveId, String followUpDate, String status);

    List<ConvertedClient> findByExecutiveIdAndStatusAndIsActive(Integer executiveId, String converted, boolean isActive);

    ConvertedClient findByIdAndIsActive(Integer convertedClientId, boolean b);

    @Query(value = "select count(cc.created_date) from converted_client cc where month(cc.created_date) =:month and year(cc.created_date) =:year and executive_id =:executiveId",nativeQuery = true)
    Integer getConvertedClientCountByExecutiveId(Integer executiveId, Integer month, Integer year);

    @Query(value = "SELECT * FROM converted_client WHERE next_followup_date =:followUpDate AND status =:status AND is_active = true AND is_completed = false",nativeQuery = true)
    List<ConvertedClient> findByNextFollowupDateAndStatusAndIsActiveAndIsCompleted(String followUpDate, String status);
}
