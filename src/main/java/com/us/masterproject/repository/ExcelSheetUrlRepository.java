package com.us.masterproject.repository;

import com.us.masterproject.model.ExcelSheetUrl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExcelSheetUrlRepository extends JpaRepository<ExcelSheetUrl,Integer> {
    List<ExcelSheetUrl> getAllExcelSheetUrlByAdminId(Integer adminId);

    List<ExcelSheetUrl> findByAdminId(Integer adminId);
}
