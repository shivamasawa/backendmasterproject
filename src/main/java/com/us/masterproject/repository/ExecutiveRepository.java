package com.us.masterproject.repository;

import com.us.masterproject.model.Executive;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ExecutiveRepository extends JpaRepository<Executive,Integer> {
    Executive findByUserName(String userName);

    Optional<Executive> findByIdAndIsActive(Integer executiveId, boolean b);

    @Query(value = "SELECT * FROM executive WHERE admin =:adminId AND is_active= true",nativeQuery = true)
    List<Executive> getAllExecutiveByAdminId(Integer adminId);

    Executive findByContactNumber(String contactNumber);

    Executive findByEmailId(String emailId);
}
