package com.us.masterproject.repository;

import com.us.masterproject.model.Admin;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AdminRepository extends JpaRepository<Admin,Integer> {

    Admin findByUserName(String userName);

    Admin findByEmailId(String emailId);

    Admin findByContactNumber(String contactNumber);

    @Query(value = "SELECT * FROM admin a where is_active = true order by a.id DESC",nativeQuery = true)
    List<Admin> getAllAdminAndIsActive();
}
