package com.us.masterproject.dto;

import javax.validation.constraints.NotEmpty;

import lombok.Data;

@Data
public class LoginReq {

    @NotEmpty(message = "user name should not be empty")
    private String userName;

    @NotEmpty(message = "password should not be empty")
    private String password;

    @NotEmpty(message = "password should not be empty")
    private Integer roleId;

    private String logInTime;
}
