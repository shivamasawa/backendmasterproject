package com.us.masterproject.dto;

import com.us.masterproject.model.Role;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class UserResponse {

    private Integer id;

    private String createdDate;

    private String createdBy;

    private String email;

    private String userName;

    private Role role;

    private String companyLogoName;

    private List<String> excelSheetUrl;
}
