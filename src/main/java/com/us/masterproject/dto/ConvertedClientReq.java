package com.us.masterproject.dto;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class ConvertedClientReq {

    private Integer convertedClientId;

    private Integer pendingClientId;

    private Integer executiveId;

    private String clientName;

    private String contactNumber;

    private String universityName;

    private String subject;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date nextFollowupDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateOfBirth;

    private String remark;

    private String email;

    private String otherDetails;

    private Integer freshClientId;

    private String topic;

    private String wordCount;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dueDate;

    private String noOfFile;

    private String totalAmount;

    private String remainingAmount;

    private String codeSecret;

    private String services;

    private String installmentAmount;

    private String transactionId;

    private String countryCode;

    private String reference;

}
