package com.us.masterproject.dto;

import lombok.Data;

import javax.persistence.Column;

@Data
public class AttendanceResponse {

    private Integer attendanceId;

    private String logInTime;

    private String logOutTime;

    private String executiveName;

    private String createdDate;

    private String createdBy;

    private String updatedDate;

    private String updatedBy;
}
