package com.us.masterproject.dto;

import lombok.Data;

@Data
public class AddAdminReq {

   private  Integer adminId;

    private String firstName;

    private String lastName;

    private String userName;

    private String contactNumber;

    private String companyName;

    private String password;

    private String emailId;

    private String companyLogoUrl;

    private String excelSheetUrl;
}
