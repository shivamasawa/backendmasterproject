package com.us.masterproject.dto;

import lombok.Data;

import java.util.Date;
@Data
public class ExecutiveResponse {

    private Integer executiveId;

    private String firstName;

    private String lastName;

    private String userName;

    private String password;

    private String contactNumber;

    private String remark;

    private String emailId;

    private Boolean status;

    private Boolean isActive;

    private String createdBy;

    private String createdDate;

    private String updatedBy;

    private String updatedDate;

}
