package com.us.masterproject.dto;

import lombok.Data;

import java.util.Date;

@Data
public class ClientsResponse {

	private Integer clientId;
	
	private String clientName;

	private String countryCode;

	private String contactNumber;

	private String reference;

	private String universityName;

	private String subject;

	private String nextFollowupDate;

	private String dateOfBirth;

	private String remark;

	private String email;

	private String otherDetails;

	private String status;

	private Boolean isActive;

	private Integer freshClientId;

	private String createdDate;

	private String createdBy;

	private String updatedDate;

	private String updatedBy;
}
