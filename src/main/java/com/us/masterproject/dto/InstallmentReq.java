package com.us.masterproject.dto;

import lombok.Data;

@Data
public class InstallmentReq {

    private Integer installmentId;

    private String installmentAmount;

    private String transactionId;
}
