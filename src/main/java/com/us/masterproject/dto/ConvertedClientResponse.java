package com.us.masterproject.dto;

import com.us.masterproject.model.Installment;
import lombok.Data;

import java.util.List;

@Data
public class ConvertedClientResponse {

    private Integer convertedClientId;

    private String clientName;

    private String countryCode;

    private String contactNumber;

    private String universityName;

    private String subject;

    private String nextFollowupDate;

    private String dateOfBirth;

    private String remark;

    private String email;

    private String otherDetails;

    private String topic;

    private String wordCount;

    private String dueDate;

    private String noOfFile;

    private String totalAmount;

    private String remainingAmount;

    private String reference;

    private String codeSecret;

    private String services;

    private String status;

    private Boolean isActive;

    private Boolean isCompleted;

    private List<InstallmentResponse> installmentResponses;

    private String createdDate;

    private String createdBy;

    private String updatedDate;

    private String updatedBy;
}
