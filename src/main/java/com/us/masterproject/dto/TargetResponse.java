package com.us.masterproject.dto;

import lombok.Data;

@Data
public class TargetResponse {

    private Integer targetId;

    private Integer noOfTarget;

    private String targetDate;

    private String executiveName;

    private Integer executiveId;

    private String createdBy;

    private String createdDate;

    private String updatedBy;

    private String updatedDate;
}
