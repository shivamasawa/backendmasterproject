package com.us.masterproject.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

@Data
public class AddClientReq {

	private Integer clientId;

	private Integer executiveId;

	@NotEmpty(message = "client name should not be empty")
	private String clientName;

	@NotEmpty(message = "contact number should not be empty")
	private String contactNumber;

	@NotEmpty(message = "university name should not be empty")
	private String universityName;

	@NotEmpty(message = "subject should not be empty")
	private String subject;

	@NotEmpty(message = "next Follow up date should not be empty")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date nextFollowupDate;

	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date dateOfBirth;

	private String remark;

	@NotEmpty(message = "email should not be empty")
	private String email;

	private String otherDetails;

	private String countryCode;

	private String reference;

	private Integer freshClientId;

	}
