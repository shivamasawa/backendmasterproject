package com.us.masterproject.dto;

import com.us.masterproject.model.Executive;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Data
public class AddTargetReq {

    private Integer targetId;

    private Integer noOfTarget;

    private String targetDate;

    private Integer executiveId;

}
