package com.us.masterproject.dto;

import lombok.Data;

@Data
public class ExcelSheetUrlDto {

    private Integer id;
    private String excelSheetUrl;
    private String excelSheetTitle;
    private Integer adminId;
}
