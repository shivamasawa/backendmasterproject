package com.us.masterproject.dto;

import com.us.masterproject.model.Executive;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.Date;

@Data
public class AddEodFormReq {

    private Integer eodFormId;

    private Integer pendingClientAdded;

    private Integer convertedClientAdded;

    private Integer pendingTaskClear;

    private String remark;

    private String logOutTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date eodDate;

    private Integer executiveId;

}
