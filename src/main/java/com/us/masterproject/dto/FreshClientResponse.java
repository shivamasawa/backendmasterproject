package com.us.masterproject.dto;

import lombok.Data;

@Data
public class FreshClientResponse {

    private Integer freshClientId;
    private String clientName;
    private String countryCode;
    private String contactNumber;
    private String status;
    private String reference;
    private Boolean isActive;

}
