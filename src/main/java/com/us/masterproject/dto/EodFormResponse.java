package com.us.masterproject.dto;

import lombok.Data;

@Data
public class EodFormResponse {

    private Integer eodFormId;

    private Integer pendingClientAdded;

    private Integer convertedClientAdded;

    private Integer pendingTaskClear;

    private String remark;

    private String logOutTime;

    private String executiveName;

    private String createdBy;

    private String updatedBy;

    private String createdDate;

    private String updatedDate;
}
