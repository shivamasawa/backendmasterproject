package com.us.masterproject.dto;

import lombok.Data;

@Data
public class AddFreshClient {

    private Integer freshClientId;

    private String clientName;

    private String contactNumber;

    private String countryCode;

    private String reference;

    private Integer executiveId;

}
