package com.us.masterproject.dto;

import lombok.Data;

import java.util.List;

@Data
public class SoftDeleteDto {

    private List<Integer> ids;
}
