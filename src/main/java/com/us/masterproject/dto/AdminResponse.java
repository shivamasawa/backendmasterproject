package com.us.masterproject.dto;

import lombok.Data;

@Data
public class AdminResponse {

	
	private Integer adminId;

    private String firstName;

    private String lastName;

    private String contactNumber;

    private String password;

    private String emailId;

    private String userName;

    private String companyName;

    private String companyLogoUrl;

    private String excelSheetUrl;

    private String createdBy;

    private String createdDate;

    private String updatedBy;

    private String updatedDate;

}
