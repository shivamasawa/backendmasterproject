package com.us.masterproject.dto;

import lombok.Data;

@Data
public class InstallmentResponse {

    private Integer installmentId;
    private String installmentAmount;
    private String transactionId;
    private String createdDate;
    private String createdBy;
    private String updatedDate;
    private String updatedBy;
}
