package com.us.masterproject.dto;

import lombok.Data;

@Data
public class AddExecutiveReq {

    private Integer executiveId;

    private String firstName;

    private String lastName;

    private String userName;

    private String password;

    private String contactNumber;

    private String remark;

    private String emailId;

    private Integer adminId;
}
