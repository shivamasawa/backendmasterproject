package com.us.masterproject.serviceImpl;

import com.us.masterproject.dto.AddEodFormReq;
import com.us.masterproject.dto.AddFreshClient;
import com.us.masterproject.dto.EodFormResponse;
import com.us.masterproject.dto.FreshClientResponse;
import com.us.masterproject.exception.InvalidDataException;
import com.us.masterproject.exception.NotFoundException;
import com.us.masterproject.model.Attendance;
import com.us.masterproject.model.EodForm;
import com.us.masterproject.model.Executive;
import com.us.masterproject.model.FreshClient;
import com.us.masterproject.repository.AttendanceRepository;
import com.us.masterproject.repository.EodFormRepository;
import com.us.masterproject.repository.ExecutiveRepository;
import com.us.masterproject.repository.FreshClientRepository;
import com.us.masterproject.service.ExecutiveService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static com.us.masterproject.utils.ModelConstant.*;

@Slf4j
@Service
public class ExecutiveServiceImpl implements ExecutiveService {

    @Autowired
    private FreshClientRepository freshClientRepository;

    @Autowired
    private ExecutiveRepository executiveRepository;

    @Autowired
    private EodFormRepository eodFormRepository;

    @Autowired
    private AttendanceRepository attendanceRepository;

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public List<FreshClient> getAllFreshClientResponseList() {
        List<FreshClient> allFreshClientResp = freshClientRepository.findAll();
        if (!CollectionUtils.isEmpty(allFreshClientResp)) {
            return allFreshClientResp;
        }
        return Collections.emptyList();
    }

    @Override
    public FreshClientResponse getFreshClientById(Integer freshClientId) throws NotFoundException {
        FreshClient freshClient = freshClientRepository.findById(freshClientId)
                .orElseThrow(() -> new NotFoundException("fresh client not found for id : " + freshClientId));
        if (!ObjectUtils.isEmpty(freshClient)) {
            FreshClientResponse freshClientResponse = new FreshClientResponse();
            BeanUtils.copyProperties(freshClient, freshClientResponse);
            freshClientResponse.setFreshClientId(freshClient.getId());
            return freshClientResponse;
        } else
            throw new NotFoundException("fresh client not found for this id: " + freshClientId);
    }

    @Override
    public List<FreshClientResponse> getAllFreshClientByExecutiveId(Integer executiveId) throws InvalidDataException {
        List<FreshClientResponse> freshClientResponses = new ArrayList<>();
        if (executiveId != null) {
            List<FreshClient> allFreshClientResp = freshClientRepository.getAllFreshClientByExecutiveId(executiveId, "freshClient");
            if (!CollectionUtils.isEmpty(allFreshClientResp)) {
                allFreshClientResp.forEach(getFreshClient -> {
                    FreshClientResponse freshClientResponse = new FreshClientResponse();
                    BeanUtils.copyProperties(getFreshClient, freshClientResponse);
                    freshClientResponse.setFreshClientId(getFreshClient.getId());
                    freshClientResponses.add(freshClientResponse);
                });
            }
            return freshClientResponses;
        }
        throw new InvalidDataException(EXECUTIVE_ID_MUST);
    }

    @Override
    public EodForm addUpdateEodForm(AddEodFormReq addEodFormReq) throws NotFoundException, InvalidDataException {
        log.info("Enter addUpdateEodForm() inside executiveServiceImpl with request: {}", addEodFormReq);
        EodForm eodForm = new EodForm();
        EodForm savedEodForm = null;
        if (!ObjectUtils.isEmpty(addEodFormReq)) {
            if (addEodFormReq.getExecutiveId() != null) {
                Executive executive = executiveRepository.findById(addEodFormReq.getExecutiveId())
                        .orElseThrow(() -> new NotFoundException("executive not found for id : " + addEodFormReq.getExecutiveId()));
                if (addEodFormReq.getEodFormId() != null) {
                    EodForm getEodForm = eodFormRepository.findById(addEodFormReq.getEodFormId())
                            .orElseThrow(() -> new NotFoundException("eod form not found form id : " + addEodFormReq.getEodFormId()));
                    BeanUtils.copyProperties(addEodFormReq, getEodForm);
                    getEodForm.setExecutive(executive);
                    getEodForm.setUpdatedBy("executive");
                    getEodForm.setUpdatedDate(new Date());
                    savedEodForm = eodFormRepository.save(getEodForm);
                    savedEodForm.setMessage(SUCCESS);
                    return savedEodForm;
                }
                if (Boolean.FALSE.equals(eodFormRepository.existsByExecutiveIdAndCreatedDate(addEodFormReq.getExecutiveId(), new Date()))) {
                    BeanUtils.copyProperties(addEodFormReq, eodForm);
                    if(addEodFormReq.getEodDate()!=null) {
                        eodForm.setEodDate(addEodFormReq.getEodDate());
                        eodForm.setExecutive(executive);
                        eodForm.setCreatedBy("executive");
                        eodForm.setCreatedDate(new Date());
                        log.info("eod form created date: " + new Date());
                        savedEodForm = eodFormRepository.save(eodForm);
                        buildAttendanceDetails(addEodFormReq.getLogOutTime(), executive);
                        savedEodForm.setMessage(SUCCESS);
                        return savedEodForm;
                    }
                    throw new InvalidDataException(EOD_DATE_MUST);
                }
                throw new InvalidDataException(EOD_ALREADY_EXIST);
            }
            throw new InvalidDataException(EXECUTIVE_ID_MUST);
        }
        throw new InvalidDataException(EOD_REQ_MUST);
    }

    private void buildAttendanceDetails(String logOutTime, Executive executive) {
        Attendance getAttendance = attendanceRepository.findByExecutiveIdAndCreatedDate(executive.getId(), new Date());
        if (!ObjectUtils.isEmpty(getAttendance)) {
            getAttendance.setLogOutTime(logOutTime);
            attendanceRepository.save(getAttendance);
        }
    }

    @Override
    public List<EodFormResponse> getAllEodFormDetailsByExecutiveId(Integer executiveId) throws NotFoundException, InvalidDataException {
        List<EodFormResponse> eodFormResponseList = new ArrayList<>();
            Executive executive;
            if (executiveId != null) {
                executive = executiveRepository.findById(executiveId).orElseThrow(() -> new NotFoundException("executive not for this id: " + executiveId));
            List<EodForm> allEodFormDetailsResp = eodFormRepository.findByExecutiveId(executiveId);
            if (!CollectionUtils.isEmpty(allEodFormDetailsResp)) {
                allEodFormDetailsResp.forEach(getEodDetails -> {
                    EodFormResponse eodFormResponse = new EodFormResponse();
                    BeanUtils.copyProperties(getEodDetails, eodFormResponse);
                    eodFormResponse.setEodFormId(getEodDetails.getId());
                    if(!ObjectUtils.isEmpty(executive)) {
                        eodFormResponse.setExecutiveName(executive.getFirstName() + " " + executive.getLastName());
                    }if (getEodDetails.getCreatedDate() != null) {
                        eodFormResponse.setCreatedDate(simpleDateFormat.format(getEodDetails.getCreatedDate()));
                    }
                    if (getEodDetails.getUpdatedDate() != null) {
                        eodFormResponse.setUpdatedDate(simpleDateFormat.format(getEodDetails.getUpdatedDate()));
                    }
                    eodFormResponseList.add(eodFormResponse);
                });
                return eodFormResponseList;
            }
            throw new NotFoundException("EOD form details not found for this executive" + executiveId);
        }
        throw new InvalidDataException(EXECUTIVE_ID_MUST);
    }

    @Override
    public Boolean getEodFormDetailsByDate(Integer executiveId, Date todayDate) {
        return eodFormRepository.existsByExecutiveIdAndCreatedDate(executiveId, todayDate);
    }

    @Override
    public FreshClient addUpdateFreshClient(AddFreshClient addFreshClient) throws NotFoundException, InvalidDataException {
        log.info("Enter addUpdateFreshClient() inside executiveServiceImpl with request: {}", addFreshClient);
        FreshClient freshClient = new FreshClient();
        FreshClient savedFreshClient;
        if (!ObjectUtils.isEmpty(addFreshClient)) {
            if (addFreshClient.getExecutiveId() != null) {
                Executive executive = executiveRepository.findById(addFreshClient.getExecutiveId())
                        .orElseThrow(() -> new NotFoundException("executive not found for id : " + addFreshClient.getExecutiveId()));
                if (addFreshClient.getFreshClientId() != null) {
                    FreshClient getFreshClient = freshClientRepository.findById(addFreshClient.getFreshClientId())
                            .orElseThrow(() -> new NotFoundException("fresh client not found form id : " + addFreshClient.getFreshClientId()));
                    BeanUtils.copyProperties(addFreshClient, getFreshClient);
                    getFreshClient.setExecutive(executive);
                    getFreshClient.setUpdatedBy("executive");
                    getFreshClient.setUpdatedDate(new Date());
                    savedFreshClient = freshClientRepository.save(getFreshClient);
                    savedFreshClient.setMessage(SUCCESS);
                    return savedFreshClient;
                }
                BeanUtils.copyProperties(addFreshClient, freshClient);
                freshClient.setExecutive(executive);
                freshClient.setStatus(FRESH_CLIENT);
                freshClient.setIsActive(true);
                freshClient.setCreatedBy("executive");
                freshClient.setCreatedDate(new Date());
                savedFreshClient = freshClientRepository.save(freshClient);
                savedFreshClient.setMessage(SUCCESS);
                return savedFreshClient;
            }
            throw new InvalidDataException(EXECUTIVE_ID_MUST);
        }
        throw new InvalidDataException(FRESH_CLIENT_REQ_MUST);
    }
}
