package com.us.masterproject.serviceImpl;

import com.us.masterproject.dto.UserResponse;
import com.us.masterproject.exception.InvalidDataException;
import com.us.masterproject.exception.NotFoundException;
import com.us.masterproject.model.Admin;
import com.us.masterproject.model.Attendance;
import com.us.masterproject.model.Executive;
import com.us.masterproject.model.Role;
import com.us.masterproject.repository.AdminRepository;
import com.us.masterproject.repository.AttendanceRepository;
import com.us.masterproject.repository.ExecutiveRepository;
import com.us.masterproject.repository.RoleRepository;
import com.us.masterproject.service.UserService;
import com.us.masterproject.utils.LoginResponseHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

import static com.us.masterproject.utils.ModelConstant.*;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    AdminRepository adminRepository;

    @Autowired
    ExecutiveRepository executiveRepository;

    @Autowired
    LoginResponseHelper loginResponseHelper;

    @Autowired
    AttendanceRepository attendanceRepository;

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public UserResponse loginUser(Integer roleId, String userName, String password, String logInTime) throws NotFoundException, InvalidDataException {
        Admin admin = null;
        Admin superAdmin = null;
        Executive executive = null;
        UserResponse userResponse = null;
        if (roleId != null) {
            Role role = null;
            role = roleRepository.findById(roleId).orElseThrow(() -> new NotFoundException("Role not found for id : " + roleId));
            switch (role.getName()) {
                case "superAdmin":
                    superAdmin = adminRepository.findByUserName(userName);
                    break;
                case "admin":
                    admin = adminRepository.findByUserName(userName);
                    break;
                case "executive":
                    executive = executiveRepository.findByUserName(userName);
                    break;
            }
            if (ObjectUtils.isEmpty(superAdmin) && role.getName().equals("superAdmin")) {
                throw new InvalidDataException("User not exists");
            }
            if (ObjectUtils.isEmpty(admin) && role.getName().equals("admin")) {
                throw new InvalidDataException("User not exists");
            }
            if (ObjectUtils.isEmpty(executive) && role.getName().equals("executive")) {
                throw new InvalidDataException("User not exists");
            }
            if (!ObjectUtils.isEmpty(superAdmin) && role.getName().equals("superAdmin")) {
                if (!superAdmin.getPassword().equals(Base64.getEncoder().encodeToString(password.getBytes()))) {
                    throw new InvalidDataException("Password mismatched");
                }
                userResponse = loginResponseHelper.buildSuperAdminResponse(superAdmin);
                if (superAdmin.getCreatedDate() != null) {
                    userResponse.setCreatedDate(simpleDateFormat.format(superAdmin.getCreatedDate()));
                }
            } else if (!ObjectUtils.isEmpty(admin)) {

                if (!admin.getPassword().equals(Base64.getEncoder().encodeToString(password.getBytes()))) {
                    throw new InvalidDataException("Password mismatched");
                }
                userResponse = loginResponseHelper.buildAdminResponse(admin);
                if (admin.getCreatedDate() != null) {
                    userResponse.setCreatedDate(simpleDateFormat.format(admin.getCreatedDate()));
                }
            } else if (!ObjectUtils.isEmpty(executive)) {
                if (!executive.getPassword().equals(Base64.getEncoder().encodeToString(password.getBytes()))) {
                    throw new InvalidDataException("Password mismatched");
                }
                userResponse = loginResponseHelper.buildExecutiveResponse(executive);
                if (executive.getCreatedDate() != null) {
                    userResponse.setCreatedDate(simpleDateFormat.format(executive.getCreatedDate()));
                }
                if (!ObjectUtils.isEmpty(executive) && logInTime != null && !logInTime.isEmpty()) {
                    Attendance getAttendance = attendanceRepository.findByExecutiveIdAndCreatedDate(executive.getId(), new Date());
                    if (ObjectUtils.isEmpty(getAttendance)) {
                        Attendance attendance = buildAttendanceDetails(executive, logInTime);
                        attendanceRepository.save(attendance);
                    }
                }
            }
        } else {
            throw new InvalidDataException("Role id must be required");
        }
        return userResponse;
    }

    @Override
    public String updatePassword(Integer id, Integer roleId, String password) throws NotFoundException, InvalidDataException {
        Admin superAdmin = null;
        Admin admin = null;
        Executive executive = null;
        if (id != null) {
            if (roleId != null) {
                if (password != null && !password.isEmpty()) {
                    Role role = roleRepository.findById(roleId).orElseThrow(() -> new NotFoundException("Role not found for id : " + roleId));
                    switch (role.getName()) {
                        case "superAdmin":
                            superAdmin = adminRepository.findById(id).orElseThrow(() -> new NotFoundException("user not found for this id: " + id));
                            break;
                        case "admin":
                            admin = adminRepository.findById(id).orElseThrow(() -> new NotFoundException("user not found for this id: " + id));
                            break;
                        case "executive":
                            executive = executiveRepository.findById(id).orElseThrow(() -> new NotFoundException("user not found for this id: " + id));
                            break;
                        default:
                            break;
                    }
                    if (!ObjectUtils.isEmpty(superAdmin) && roleId==1) {
                        superAdmin.setPassword(Base64.getEncoder().encodeToString(password.getBytes()));
                        adminRepository.save(superAdmin);
                        return SUCCESS;
                    } else if (!ObjectUtils.isEmpty(admin)) {
                        admin.setPassword(Base64.getEncoder().encodeToString(password.getBytes()));
                        adminRepository.save(admin);
                        return SUCCESS;
                    } else if (!ObjectUtils.isEmpty(executive)) {
                        executive.setPassword(Base64.getEncoder().encodeToString(password.getBytes()));
                        executiveRepository.save(executive);
                        return SUCCESS;
                    }
                }
                throw new InvalidDataException(PASSWORD_MUST);
            }
            throw new InvalidDataException(ROLE_ID_MUST);
        }
        throw new InvalidDataException(ID_MUST);
    }

    private Attendance buildAttendanceDetails(Executive executive, String logInTime) {
        Attendance attendance = new Attendance();
        attendance.setLogInTime(logInTime);
        attendance.setExecutive(executive);
        attendance.setCreatedDate(new Date());
        attendance.setCreatedBy("executive");
        return attendance;
    }
}
