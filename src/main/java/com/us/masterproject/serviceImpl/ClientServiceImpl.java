package com.us.masterproject.serviceImpl;

import com.us.masterproject.dto.*;
import com.us.masterproject.exception.InvalidDataException;
import com.us.masterproject.exception.NotFoundException;
import com.us.masterproject.model.*;
import com.us.masterproject.repository.*;
import com.us.masterproject.service.ClientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.us.masterproject.utils.ModelConstant.*;

@Service
@Slf4j
public class ClientServiceImpl implements ClientService {


    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ExecutiveRepository executiveRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private FreshClientRepository freshClientRepository;

    @Autowired
    private ConvertedClientRepository convertedClientRepository;

    @Autowired
    private InstallmentRepository installmentRepository;

    @Autowired
    private TargetRepository targetRepository;

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public Clients saveOrUpdateClient(AddClientReq addClientReq) throws NotFoundException, ParseException, InvalidDataException {
        log.info("Enter saveOrUpdateClient() inside ClientServiceImpl with request: {}", addClientReq);
        Clients clients = new Clients();
        if (!ObjectUtils.isEmpty(addClientReq)) {
            if (addClientReq.getClientId() == null && addClientReq.getExecutiveId() == null) {
                throw new InvalidDataException(EXECUTIVE_ID_MUST);
            }
            Clients client = buildClientData(addClientReq);
            if (client.getMessage().endsWith(SUCCESS)) {
                clients = clientRepository.save(client);
                clients.setMessage(SUCCESS);
                return clients;
            }
            clients.setMessage(client.getMessage());
            return clients;
        }
        throw new InvalidDataException(ADD_CLIENT_REQ_MUST);
    }

    private Clients buildClientData(AddClientReq addClientReq) throws NotFoundException, ParseException, InvalidDataException {
        Executive executive = new Executive();
        if (addClientReq.getClientId() == null && addClientReq.getExecutiveId() != null) {
            executive = executiveRepository.findById(addClientReq.getExecutiveId())
                    .orElseThrow(() -> new NotFoundException("executive not found for id : " + addClientReq.getExecutiveId()));
        }
        if (addClientReq.getClientId() != null) {
            Clients clients = clientRepository.findById(addClientReq.getClientId())
                    .orElseThrow(() -> new NotFoundException("client not found form id : " + addClientReq.getClientId()));
            clients.setClientName(addClientReq.getClientName());
            clients.setContactNumber(addClientReq.getContactNumber());
            clients.setEmail(addClientReq.getEmail());
            clients.setSubject(addClientReq.getSubject());
            clients.setUniversityName(addClientReq.getUniversityName());
            clients.setRemark(addClientReq.getRemark());
            clients.setOtherDetails(addClientReq.getOtherDetails());
            clients.setCountryCode(addClientReq.getCountryCode());
            clients.setCountryCode(addClientReq.getReference());
            clients.setDateOfBirth(addClientReq.getDateOfBirth());
            if (addClientReq.getNextFollowupDate() != null) {
                clients.setNextFollowupDate(addClientReq.getNextFollowupDate());
            }
            clients.setUpdatedBy("executive");
            clients.setUpdatedDate(new Date());
            clients.setMessage(SUCCESS);
            return clients;
        }

        if (addClientReq.getFreshClientId() != null) {
            FreshClient freshClient = freshClientRepository.findById(addClientReq.getFreshClientId())
                    .orElseThrow(() -> new NotFoundException("fresh client not found for id : " + addClientReq.getFreshClientId()));
            freshClient.setStatus(PENDING);
            freshClientRepository.save(freshClient);
        } else {
            throw new InvalidDataException(FRESH_CLIENT_ID_MUST);
        }
        return Clients.builder()
                .clientName(addClientReq.getClientName())
                .contactNumber(addClientReq.getContactNumber())
                .dateOfBirth(addClientReq.getDateOfBirth())
                .email(addClientReq.getEmail())
                .executive(executive)
                .nextFollowupDate(addClientReq.getNextFollowupDate())
                .status(PENDING)
                .isActive(true)
                .universityName(addClientReq.getUniversityName())
                .subject(addClientReq.getSubject())
                .createdBy("Client")
                .createdDate(new Date())
                .remark(addClientReq.getRemark())
                .otherDetails(addClientReq.getOtherDetails())
                .freshClientId(addClientReq.getFreshClientId() != null ? addClientReq.getFreshClientId() : null)
                .countryCode(addClientReq.getCountryCode())
                .reference(addClientReq.getReference())
                .message(SUCCESS)
                .build();
    }

    @Override
    public List<ClientsResponse> getAllClientsResponseList() {
        List<ClientsResponse> clientsResponses = new ArrayList<>();
        List<Clients> clientList = clientRepository.findAll();
        BeanUtils.copyProperties(clientList, clientsResponses);
        return clientsResponses;

    }

    @Override
    public void deleteFreshClientById(SoftDeleteDto freshClientId) throws InvalidDataException {
        if (!freshClientId.getIds().isEmpty()) {
            freshClientId.getIds().forEach(afreshClientId -> {
                try {
                    FreshClient freshClient = freshClientRepository.findById(afreshClientId)
                            .orElseThrow(() -> new NotFoundException("fresh client not found for id : " + afreshClientId));
                    freshClient.setIsActive(false);
                    freshClientRepository.save(freshClient);
                } catch (NotFoundException e) {
                    e.printStackTrace();
                }
            });
        } else
            throw new InvalidDataException("fresh client ids must be required");
    }

    @Override
    public ConvertedClient saveConvertedClient(ConvertedClientReq convertedClientReq) throws NotFoundException, ParseException, InvalidDataException {
        log.info("Enter saveConvertedClient() inside ClientServiceImpl with request: {}", convertedClientReq);
        ConvertedClient savedConvertedClient = new ConvertedClient();
        if (!ObjectUtils.isEmpty(convertedClientReq)) {
            ConvertedClient convertedClient = buildConvertedClientData(convertedClientReq);
            if (convertedClient.getMessage().endsWith(SUCCESS)) {
                savedConvertedClient = convertedClientRepository.save(convertedClient);
                if (!ObjectUtils.isEmpty(savedConvertedClient)) {
                    Installment installment = new Installment();
                    installment.setInstallmentAmount(convertedClientReq.getInstallmentAmount() != null ? convertedClientReq.getInstallmentAmount() : null);
                    installment.setTransactionId(convertedClientReq.getTransactionId() != null ? convertedClientReq.getTransactionId() : null);
                    installment.setConvertedClient(savedConvertedClient);
                    installment.setCreatedBy("executive");
                    installment.setCreatedDate(new Date());
                    installmentRepository.save(installment);
                    if (convertedClientReq.getConvertedClientId() == null) {
                        if (convertedClientReq.getFreshClientId() != null) {
                            FreshClient freshClient = freshClientRepository.findById(convertedClientReq.getFreshClientId())
                                    .orElseThrow(() -> new NotFoundException("fresh client not found for id : " + convertedClientReq.getFreshClientId()));
                            freshClient.setStatus(CONVERTED);
                            freshClientRepository.save(freshClient);
                        } else {
                            throw new InvalidDataException(FRESH_CLIENT_ID_MUST);
                        }
                        if (convertedClientReq.getPendingClientId() != null) {
                            Clients clients = clientRepository.findById(convertedClientReq.getPendingClientId())
                                    .orElseThrow(() -> new NotFoundException("pending client not found for id : " + convertedClientReq.getPendingClientId()));
                            clients.setStatus(CONVERTED);
                            clientRepository.save(clients);
                        } else {
                            throw new InvalidDataException(PENDING_CLIENT_ID_MUST);
                        }
                    }
                    savedConvertedClient.setMessage(SUCCESS);
                    return savedConvertedClient;
                }
                throw new InvalidDataException("Converted client not added");
            }
            savedConvertedClient.setMessage(convertedClient.getMessage());
            return savedConvertedClient;
        }
        throw new InvalidDataException(ADD_CONVERTED_CLIENT_REQ_MUST);
    }

    @Override
    public List<ClientsResponse> getAllPendingClientByDate(Integer executiveId, Date followUpDate) throws ParseException, NotFoundException {
        List<ClientsResponse> clientsResponses = new ArrayList<>();
        String datePattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat(datePattern);
        String date = simpleDateFormat1.format(followUpDate);
        List<Clients> clientList = clientRepository.findByExecutiveIdAndNextFollowupDateAndStatusAndIsActive(executiveId, date, "pending");
        if (!CollectionUtils.isEmpty(clientList)) {
            clientList.forEach(getClient -> {
                ClientsResponse clientsResponse = new ClientsResponse();
                BeanUtils.copyProperties(getClient, clientsResponse);
                clientsResponse.setClientId(getClient.getId());
                clientsResponse.setCreatedBy(getClient.getCreatedBy());
                clientsResponse.setUpdatedBy(getClient.getUpdatedBy());
                if (getClient.getDateOfBirth() != null) {
                    clientsResponse.setDateOfBirth((simpleDateFormat.format(getClient.getDateOfBirth())));
                }
                if (getClient.getNextFollowupDate() != null) {
                    clientsResponse.setNextFollowupDate(simpleDateFormat.format(getClient.getNextFollowupDate()));
                }
                if (getClient.getCreatedDate() != null) {
                    clientsResponse.setCreatedDate(simpleDateFormat.format(getClient.getCreatedDate()));
                }
                if (getClient.getUpdatedDate() != null) {
                    clientsResponse.setUpdatedDate(simpleDateFormat.format(getClient.getUpdatedDate()));
                }
                clientsResponses.add(clientsResponse);
            });
            return clientsResponses;

        }
        throw new NotFoundException("pending clients not found for this date");
    }

    @Override
    public List<ConvertedClientResponse> getAllConvertedClientByDate(Integer executiveId, Date followUpDate) throws NotFoundException {
        List<ConvertedClientResponse> convertedClientResponseList = new ArrayList<>();
        String datePattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat(datePattern);
        String followDate = simpleDateFormat1.format(followUpDate);
        List<ConvertedClient> convertedClients = convertedClientRepository.getAllConvertedClientByDate(executiveId, followDate, "converted");
        if (!CollectionUtils.isEmpty(convertedClients)) {
            convertedClients.forEach(getConvertedClient -> {
                List<InstallmentResponse> installmentResponseList = new ArrayList<>();
                ConvertedClientResponse convertedClientResponse = new ConvertedClientResponse();
                BeanUtils.copyProperties(getConvertedClient, convertedClientResponse);
                convertedClientResponse.setConvertedClientId(getConvertedClient.getId());
                List<Installment> installment = installmentRepository.findByConvertedClientId(getConvertedClient.getId());
                if (!installment.isEmpty()) {
                    installment.forEach(getInstallment -> {
                        InstallmentResponse installmentResponse = new InstallmentResponse();
                        BeanUtils.copyProperties(getInstallment, installmentResponse);
                        installmentResponse.setInstallmentId(getInstallment.getId());
                        if (getInstallment.getCreatedDate() != null) {
                            installmentResponse.setCreatedDate(simpleDateFormat.format(getInstallment.getCreatedDate()));
                        }
                        if (getInstallment.getUpdatedDate() != null) {
                            installmentResponse.setUpdatedDate(simpleDateFormat.format(getInstallment.getUpdatedDate()));
                        }
                        installmentResponseList.add(installmentResponse);
                    });
                    convertedClientResponse.setInstallmentResponses(installmentResponseList);
                }
                convertedClientResponse.setCreatedBy(getConvertedClient.getCreatedBy());
                convertedClientResponse.setUpdatedBy(getConvertedClient.getUpdatedBy());
                if (getConvertedClient.getDateOfBirth() != null) {
                    convertedClientResponse.setDateOfBirth((simpleDateFormat.format(getConvertedClient.getDateOfBirth())));
                }
                if (getConvertedClient.getNextFollowupDate() != null) {
                    convertedClientResponse.setNextFollowupDate(simpleDateFormat.format(getConvertedClient.getNextFollowupDate()));
                }
                if (getConvertedClient.getDueDate() != null) {
                    convertedClientResponse.setDueDate(simpleDateFormat.format(getConvertedClient.getDueDate()));
                }
                if (getConvertedClient.getCreatedDate() != null) {
                    convertedClientResponse.setCreatedDate(simpleDateFormat.format(getConvertedClient.getCreatedDate()));
                }
                if (getConvertedClient.getUpdatedDate() != null) {
                    convertedClientResponse.setUpdatedDate(simpleDateFormat.format(getConvertedClient.getUpdatedDate()));
                }
                convertedClientResponseList.add(convertedClientResponse);
            });
            return convertedClientResponseList;
        }
        throw new NotFoundException("converted clients not found for this date");
    }

    @Override
    public List<ConvertedClientResponse> getAllConvertedClientByExecutiveId(Integer executiveId) throws NotFoundException {
        List<ConvertedClientResponse> convertedClientResponseList = new ArrayList<>();
        List<ConvertedClient> convertedClients = convertedClientRepository.findByExecutiveIdAndStatusAndIsActive(executiveId, "converted", true);
        if (!CollectionUtils.isEmpty(convertedClients)) {
            for (ConvertedClient getConvertedClient : convertedClients) {
                List<InstallmentResponse> installmentResponseList = new ArrayList<>();
                List<Installment> installment;
                ConvertedClientResponse convertedClientResponse = new ConvertedClientResponse();
                BeanUtils.copyProperties(getConvertedClient, convertedClientResponse);
                convertedClientResponse.setConvertedClientId(getConvertedClient.getId());
                installment = installmentRepository.findByConvertedClientId(getConvertedClient.getId());
                if (installment != null && !installment.isEmpty()) {
                    installment.forEach(getInstallment -> {
                        InstallmentResponse installmentResponse = new InstallmentResponse();
                        BeanUtils.copyProperties(getInstallment, installmentResponse);
                        installmentResponse.setInstallmentId(getInstallment.getId());
                        if (getInstallment.getCreatedDate() != null) {
                            installmentResponse.setCreatedDate(simpleDateFormat.format(getInstallment.getCreatedDate()));
                        }
                        if (getInstallment.getUpdatedDate() != null) {
                            installmentResponse.setUpdatedDate(simpleDateFormat.format(getInstallment.getUpdatedDate()));
                        }
                        installmentResponseList.add(installmentResponse);
                        convertedClientResponse.setInstallmentResponses(installmentResponseList);
                    });
                }
                convertedClientResponse.setCreatedBy(getConvertedClient.getCreatedBy());
                convertedClientResponse.setUpdatedBy(getConvertedClient.getUpdatedBy());
                if (getConvertedClient.getDateOfBirth() != null) {
                    convertedClientResponse.setDateOfBirth((simpleDateFormat.format(getConvertedClient.getDateOfBirth())));
                }
                if (getConvertedClient.getNextFollowupDate() != null) {
                    convertedClientResponse.setNextFollowupDate(simpleDateFormat.format(getConvertedClient.getNextFollowupDate()));
                }
                if (getConvertedClient.getDueDate() != null) {
                    convertedClientResponse.setDueDate(simpleDateFormat.format(getConvertedClient.getDueDate()));
                }
                if (getConvertedClient.getCreatedDate() != null) {
                    convertedClientResponse.setCreatedDate(simpleDateFormat.format(getConvertedClient.getCreatedDate()));
                }
                if (getConvertedClient.getUpdatedDate() != null) {
                    convertedClientResponse.setUpdatedDate(simpleDateFormat.format(getConvertedClient.getUpdatedDate()));
                }
                convertedClientResponseList.add(convertedClientResponse);
            }
            return convertedClientResponseList;
        }
        throw new NotFoundException("converted client not found for this executive");
    }

    @Override
    public List<ClientsResponse> getAllPendingClientByExecutiveId(Integer executiveId) throws NotFoundException {
        List<ClientsResponse> clientsResponses = new ArrayList<>();
        List<Clients> clientList = clientRepository.findByExecutiveIdAndStatusAndIsActive(executiveId, "pending", true);
        if (!CollectionUtils.isEmpty(clientList)) {
            clientList.forEach(getClient -> {
                ClientsResponse clientsResponse = new ClientsResponse();
                BeanUtils.copyProperties(getClient, clientsResponse);
                clientsResponse.setClientId(getClient.getId());
                clientsResponse.setCreatedBy(getClient.getCreatedBy());
                clientsResponse.setUpdatedBy(getClient.getUpdatedBy());
                if (getClient.getDateOfBirth() != null) {
                    clientsResponse.setDateOfBirth((simpleDateFormat.format(getClient.getDateOfBirth())));
                }
                if (getClient.getNextFollowupDate() != null) {
                    clientsResponse.setNextFollowupDate(simpleDateFormat.format(getClient.getNextFollowupDate()));
                }
                if (getClient.getCreatedDate() != null) {
                    clientsResponse.setCreatedDate(simpleDateFormat.format(getClient.getCreatedDate()));
                }
                if (getClient.getUpdatedDate() != null) {
                    clientsResponse.setUpdatedDate(simpleDateFormat.format(getClient.getUpdatedDate()));
                }
                clientsResponses.add(clientsResponse);
            });
            return clientsResponses;

        }
        throw new NotFoundException("pending client not found for this executive");
    }

    @Override
    public ClientsResponse getPendingClientById(Integer pendingClientId) throws NotFoundException {
        ClientsResponse clientsResponse = new ClientsResponse();
        Clients clients = clientRepository.findByIdAndIsActive(pendingClientId, true);
        if (!ObjectUtils.isEmpty(clients)) {
            BeanUtils.copyProperties(clients, clientsResponse);
            clientsResponse.setClientId(clients.getId());
            clientsResponse.setCreatedBy(clients.getCreatedBy());
            clientsResponse.setUpdatedBy(clients.getUpdatedBy());
            if (clients.getDateOfBirth() != null) {
                clientsResponse.setDateOfBirth((simpleDateFormat.format(clients.getDateOfBirth())));
            }
            if (clients.getNextFollowupDate() != null) {
                clientsResponse.setNextFollowupDate(simpleDateFormat.format(clients.getNextFollowupDate()));
            }
            if (clients.getCreatedDate() != null) {
                clientsResponse.setCreatedDate(simpleDateFormat.format(clients.getCreatedDate()));
            }
            if (clients.getUpdatedDate() != null) {
                clientsResponse.setUpdatedDate(simpleDateFormat.format(clients.getUpdatedDate()));
            }
            return clientsResponse;
        }
        throw new NotFoundException("pending client not found for this id: " + pendingClientId);
    }

    @Override
    public ConvertedClientResponse getConvertedClientById(Integer convertedClientId) throws NotFoundException {
        List<InstallmentResponse> installmentResponseList = new ArrayList<>();
        ConvertedClientResponse convertedClientResponse = new ConvertedClientResponse();
        ConvertedClient getConvertedClient = convertedClientRepository.findByIdAndIsActive(convertedClientId, true);
        if (!ObjectUtils.isEmpty(getConvertedClient)) {
            BeanUtils.copyProperties(getConvertedClient, convertedClientResponse);
            convertedClientResponse.setConvertedClientId(getConvertedClient.getId());
            List<Installment> installment = installmentRepository.findByConvertedClientId(getConvertedClient.getId());
            if (!installment.isEmpty()) {
                installment.forEach(getInstallment -> {
                    InstallmentResponse installmentResponse = new InstallmentResponse();
                    BeanUtils.copyProperties(getInstallment, installmentResponse);
                    installmentResponse.setInstallmentId(getInstallment.getId());
                    if (getInstallment.getCreatedDate() != null) {
                        installmentResponse.setCreatedDate(simpleDateFormat.format(getInstallment.getCreatedDate()));
                    }
                    if (getInstallment.getUpdatedDate() != null) {
                        installmentResponse.setUpdatedDate(simpleDateFormat.format(getInstallment.getUpdatedDate()));
                    }
                    installmentResponseList.add(installmentResponse);
                });
                convertedClientResponse.setInstallmentResponses(installmentResponseList);
            }
            convertedClientResponse.setCreatedBy(getConvertedClient.getCreatedBy());
            convertedClientResponse.setUpdatedBy(getConvertedClient.getUpdatedBy());
            if (getConvertedClient.getDateOfBirth() != null) {
                convertedClientResponse.setDateOfBirth((simpleDateFormat.format(getConvertedClient.getDateOfBirth())));
            }
            if (getConvertedClient.getNextFollowupDate() != null) {
                convertedClientResponse.setNextFollowupDate(simpleDateFormat.format(getConvertedClient.getNextFollowupDate()));
            }
            if (getConvertedClient.getDueDate() != null) {
                convertedClientResponse.setDueDate(simpleDateFormat.format(getConvertedClient.getDueDate()));
            }
            if (getConvertedClient.getCreatedDate() != null) {
                convertedClientResponse.setCreatedDate(simpleDateFormat.format(getConvertedClient.getCreatedDate()));
            }
            if (getConvertedClient.getUpdatedDate() != null) {
                convertedClientResponse.setUpdatedDate(simpleDateFormat.format(getConvertedClient.getUpdatedDate()));
            }
            return convertedClientResponse;
        }
        throw new NotFoundException("converted client not found for this id: " + convertedClientId);
    }

    @Override
    public Installment updateInstallment(InstallmentReq installmentReq) throws NotFoundException, InvalidDataException {
        Installment installments = new Installment();
        Installment updatedInstallment;
        if (installmentReq.getInstallmentId() != null) {
            Installment installment = installmentRepository.findById(installmentReq.getInstallmentId())
                    .orElseThrow(() -> new NotFoundException("installment details  not found form id : " + installmentReq.getInstallmentId()));
            BeanUtils.copyProperties(installmentReq, installment);
            installment.setUpdatedBy("executive");
            installment.setUpdatedDate(new Date());
            updatedInstallment = installmentRepository.save(installment);
            updatedInstallment.setMessage(SUCCESS);
            return updatedInstallment;
        }
        throw new InvalidDataException(INSTALLMENT_ID_MUST);
    }

    @Override
    public TargetResponse getTargetByDate(Integer executiveId, String targetDate) throws NotFoundException {
        TargetResponse targetResponse = null;
        Target allTargetResp = targetRepository.findByExecutiveIdAndTargetDate(executiveId, targetDate);
        if (!ObjectUtils.isEmpty(allTargetResp)) {
            targetResponse = new TargetResponse();
            BeanUtils.copyProperties(allTargetResp, targetResponse);
            targetResponse.setTargetId(allTargetResp.getId());
            targetResponse.setCreatedBy(allTargetResp.getCreatedBy());
            targetResponse.setUpdatedBy(allTargetResp.getUpdatedBy());
            if (allTargetResp.getCreatedDate() != null) {
                targetResponse.setCreatedDate(simpleDateFormat.format(allTargetResp.getCreatedDate()));
            }
            if (allTargetResp.getUpdatedDate() != null) {
                targetResponse.setUpdatedDate(simpleDateFormat.format(allTargetResp.getUpdatedDate()));
            }
            return targetResponse;
        }
        throw new NotFoundException("Target not found for this executive");
    }

    @Override
    public Integer getConvertedClientCountByExecutiveId(Integer executiveId, Integer month, Integer year) {
        return convertedClientRepository.getConvertedClientCountByExecutiveId(executiveId, month, year);
    }

    @Override
    public void changeCompletedStatusByConvertedClientId(Integer convertedClientId, Boolean status) throws NotFoundException {
        ConvertedClient convertedClient = convertedClientRepository.findById(convertedClientId)
                .orElseThrow(() -> new NotFoundException("converted client not found for id : " + convertedClientId));
        convertedClient.setIsCompleted(status);
        convertedClient.setUpdatedDate(new Date());
        convertedClientRepository.save(convertedClient);
    }

    private ConvertedClient buildConvertedClientData(ConvertedClientReq convertedClientReq) throws NotFoundException, ParseException, InvalidDataException {
        if (convertedClientReq.getConvertedClientId() != null) {
            ConvertedClient convertedClient = convertedClientRepository.findById(convertedClientReq.getConvertedClientId())
                    .orElseThrow(() -> new NotFoundException("converted client not found form id : " + convertedClientReq.getConvertedClientId()));
            BeanUtils.copyProperties(convertedClientReq, convertedClient);
            if (convertedClientReq.getDateOfBirth() != null) {
                convertedClient.setDateOfBirth(convertedClientReq.getDateOfBirth());
            }
            if (convertedClientReq.getNextFollowupDate() != null) {
                convertedClient.setNextFollowupDate(convertedClientReq.getNextFollowupDate());
            }
            if (convertedClientReq.getDueDate() != null) {
                convertedClient.setDueDate(convertedClientReq.getDueDate());
            }
            convertedClient.setUpdatedBy("executive");
            convertedClient.setUpdatedDate(new Date());
            convertedClient.setMessage(SUCCESS);
            return convertedClient;
        }
        if (convertedClientReq.getExecutiveId() != null) {
            Executive executive = executiveRepository.findById(convertedClientReq.getExecutiveId())
                    .orElseThrow(() -> new NotFoundException("executive not found for id : " + convertedClientReq.getExecutiveId()));
            return ConvertedClient.builder()
                    .clientName(convertedClientReq.getClientName())
                    .contactNumber(convertedClientReq.getContactNumber())
                    .email(convertedClientReq.getEmail())
                    .dueDate(convertedClientReq.getDueDate())
                    .nextFollowupDate(convertedClientReq.getNextFollowupDate())
                    .dateOfBirth(convertedClientReq.getDateOfBirth())
                    .executive(executive)
                    .status(CONVERTED)
                    .isActive(true)
                    .isCompleted(false)
                    .universityName(convertedClientReq.getUniversityName())
                    .subject(convertedClientReq.getSubject())
                    .createdBy("executive")
                    .createdDate(new Date())
                    .remark(convertedClientReq.getRemark())
                    .otherDetails(convertedClientReq.getOtherDetails())
                    .topic(convertedClientReq.getTopic())
                    .codeSecret(convertedClientReq.getCodeSecret())
                    .services(convertedClientReq.getServices())
                    .noOfFile(convertedClientReq.getNoOfFile())
                    .remainingAmount(convertedClientReq.getRemainingAmount())
                    .totalAmount(convertedClientReq.getTotalAmount())
                    .wordCount(convertedClientReq.getWordCount())
                    .reference(convertedClientReq.getReference())
                    .countryCode(convertedClientReq.getCountryCode())
                    .message(SUCCESS)
                    .build();
        }
        throw new InvalidDataException(EXECUTIVE_ID_MUST);
    }
}
