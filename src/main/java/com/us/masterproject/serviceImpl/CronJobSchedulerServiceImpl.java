package com.us.masterproject.serviceImpl;

import com.us.masterproject.model.Clients;
import com.us.masterproject.model.ConvertedClient;
import com.us.masterproject.repository.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
@Slf4j
@Component
public class CronJobSchedulerServiceImpl {


    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ExecutiveRepository executiveRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private FreshClientRepository freshClientRepository;

    @Autowired
    private ConvertedClientRepository convertedClientRepository;

    @Autowired
    private InstallmentRepository installmentRepository;

    @Autowired
    private TargetRepository targetRepository;

    @Scheduled(cron = " 0 30 17 ? * *")
    public void performPendingClientsForwardDate() {
        log.info("Start performPendingClientsForwardDate scheduler at 5:30PM: "+new Date());
        List<Clients> clientsLists = new ArrayList<>();
        List<ConvertedClient> convertedClientList = new ArrayList<>();
        int noOfDayToAdd = 1;
        Date today = new Date();
        Date tomorrow = new Date(today.getYear(), today.getMonth(), today.getDate() + noOfDayToAdd);
        String datePattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat(datePattern);
        String date = simpleDateFormat1.format(today);
        log.info("performPendingClientsForwardDate scheduler today date: "+date);
        List<Clients> clientList = clientRepository.findByNextFollowupDateAndStatusAndIsActive(date, "pending");
        if (!CollectionUtils.isEmpty(clientList)) {
            clientList.forEach(getClient -> {
                Clients clients = new Clients();
                BeanUtils.copyProperties(getClient, clients);
                clients.setNextFollowupDate(tomorrow);
                clients.setUpdatedDate(new Date());
                clients.setUpdatedBy("cron-scheduler-1");
                clientsLists.add(clients);
            });
            clientRepository.saveAll(clientsLists);
        }
        List<ConvertedClient> convertedClients = convertedClientRepository.findByNextFollowupDateAndStatusAndIsActiveAndIsCompleted(date, "converted");
        if (!CollectionUtils.isEmpty(convertedClients)) {
            ConvertedClient convertedClient = new ConvertedClient();
            convertedClients.forEach(getConvertedClient -> {
                BeanUtils.copyProperties(getConvertedClient, convertedClient);
                convertedClient.setNextFollowupDate(tomorrow);
                convertedClient.setUpdatedDate(new Date());
                convertedClient.setUpdatedBy("cron-scheduler-1");
                convertedClientList.add(convertedClient);
            });
            log.info("End performPendingClientsForwardDate scheduler at: "+new Date());
            convertedClientRepository.saveAll(convertedClientList);
        }
    }

//    @Scheduled(cron = " 0 30 4 ? * *")
//    public void performConvertedClientsForwardDate() {
//        log.info("Start performConvertedClientsForwardDate scheduler 2 at 4:30AM: "+new Date());
//        List<Clients> clientsLists = new ArrayList<>();
//        List<ConvertedClient> convertedClientList = new ArrayList<>();
//        int noOfDayToAdd = 1;
//        Date today = new Date();
//        Date tomorrow = new Date(today.getYear(), today.getMonth(), today.getDate() + noOfDayToAdd);
//        String datePattern = "yyyy-MM-dd";
//        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat(datePattern);
//        String date = simpleDateFormat1.format(today);
//        log.info("performConvertedClientsForwardDate scheduler today date: "+date);
//        List<Clients> clientList = clientRepository.findByNextFollowupDateAndStatusAndIsActive(date, "pending");
//        if (!CollectionUtils.isEmpty(clientList)) {
//            clientList.forEach(getClient -> {
//                Clients clients = new Clients();
//                BeanUtils.copyProperties(getClient, clients);
//                clients.setNextFollowupDate(tomorrow);
//                clients.setUpdatedDate(new Date());
//                clients.setUpdatedBy("cron-scheduler-2");
//                clientsLists.add(clients);
//            });
//            clientRepository.saveAll(clientsLists);
//        }
//        List<ConvertedClient> convertedClients = convertedClientRepository.findByNextFollowupDateAndStatusAndIsActive(date, "converted");
//        if (!CollectionUtils.isEmpty(convertedClients)) {
//            ConvertedClient convertedClient = new ConvertedClient();
//            convertedClients.forEach(getConvertedClient -> {
//                BeanUtils.copyProperties(getConvertedClient, convertedClient);
//                convertedClient.setNextFollowupDate(tomorrow);
//                convertedClient.setUpdatedDate(new Date());
//                convertedClient.setUpdatedBy("cron-scheduler-2");
//                convertedClientList.add(convertedClient);
//            });
//            log.info("End performConvertedClientsForwardDate scheduler at: "+new Date());
//            convertedClientRepository.saveAll(convertedClientList);
//        }
//    }
}
