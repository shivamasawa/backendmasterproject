package com.us.masterproject.serviceImpl;

import com.us.masterproject.dto.AddAdminReq;
import com.us.masterproject.dto.AdminResponse;
import com.us.masterproject.exception.InvalidDataException;
import com.us.masterproject.exception.NotFoundException;
import com.us.masterproject.model.Admin;
import com.us.masterproject.model.Role;
import com.us.masterproject.repository.AdminRepository;
import com.us.masterproject.repository.RoleRepository;
import com.us.masterproject.service.SuperAdminService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.text.SimpleDateFormat;
import java.util.*;

import static com.us.masterproject.utils.ModelConstant.*;

@Service
@Slf4j
public class SuperAdminServiceImpl implements SuperAdminService {
    private String FilePath = "resources/";
    @Autowired
    private AdminRepository adminRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private FileStorageServiceImpl fileStorageService;

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public Admin addUpdateAdmin(AddAdminReq addAdminReq) throws NotFoundException, InvalidDataException {
        log.info("Enter addUpdateAdmin() inside adminServiceImpl with request: {}", addAdminReq);
        Admin admin = new Admin();
        if (!ObjectUtils.isEmpty(addAdminReq)) {
            Admin validAdminMsg = buildAdminValidation(addAdminReq);
            if (validAdminMsg.getMessage().endsWith(SUCCESS)) {
                if (addAdminReq.getAdminId() != null) {
                    Admin updatedAdmin = null;
                    Admin adminFromDb = adminRepository.findById(addAdminReq.getAdminId())
                            .orElseThrow(() -> new NotFoundException("admin not found for id : " + addAdminReq.getAdminId()));
                    addAdminReq.setPassword(adminFromDb.getPassword() != null ? Base64.getEncoder().encodeToString(adminFromDb.getPassword().getBytes()) : null);
                    BeanUtils.copyProperties(addAdminReq, adminFromDb);
                    adminFromDb.setUpdatedBy("Admin");
                    adminFromDb.setUpdatedDate(new Date());
                    updatedAdmin = adminRepository.save(adminFromDb);
                    updatedAdmin.setMessage(SUCCESS);
                    return updatedAdmin;
                }
                Admin savedAdmin = null;
                Role role = roleRepository.findById(2).orElseThrow(() -> new NotFoundException("Role not found for id : " + 2));
                BeanUtils.copyProperties(addAdminReq, admin);
                admin.setRole(role);
                admin.setIsActive(true);
                admin.setPassword(Base64.getEncoder().encodeToString(addAdminReq.getPassword().getBytes()));
                admin.setCreatedBy("Admin");
                admin.setCreatedDate(new Date());
                savedAdmin = adminRepository.save(admin);
                savedAdmin.setMessage(SUCCESS);
                return savedAdmin;
            }
            throw new InvalidDataException(validAdminMsg.getMessage());
        }
        throw new InvalidDataException(ADD_ADMIN_REQ_MUST);
    }


    @Override
    public AdminResponse getAdminById(Integer adminId) throws NotFoundException {
        Admin admin = adminRepository.findById(adminId)
                .orElseThrow(() -> new NotFoundException("admin not found for id : " + adminId));
        AdminResponse adminResponse = new AdminResponse();
        BeanUtils.copyProperties(admin, adminResponse);
        adminResponse.setAdminId(admin.getId());
        if (admin.getCreatedDate() != null) {
            adminResponse.setCreatedDate(simpleDateFormat.format(admin.getCreatedDate()));
        }
        if (admin.getUpdatedDate() != null) {
            adminResponse.setUpdatedDate(simpleDateFormat.format(admin.getUpdatedDate()));
        }
        adminResponse.setCreatedBy(admin.getCreatedBy());
        adminResponse.setUpdatedBy(admin.getUpdatedBy());
        return adminResponse;
    }

    @Override
    public List<AdminResponse> getAllAdmin() {
        List<AdminResponse> adminResponses = new ArrayList<>();
        List<Admin> adminList = adminRepository.getAllAdminAndIsActive();
        if (!CollectionUtils.isEmpty(adminList)) {
            adminList.forEach(getAdmin -> {
                AdminResponse adminResponse = new AdminResponse();
                BeanUtils.copyProperties(getAdmin, adminResponse);
                adminResponse.setAdminId(getAdmin.getId());
                if (getAdmin.getCreatedDate() != null) {
                    adminResponse.setCreatedDate(simpleDateFormat.format(getAdmin.getCreatedDate()));
                }
                if (getAdmin.getUpdatedDate() != null) {
                    adminResponse.setUpdatedDate(simpleDateFormat.format(getAdmin.getUpdatedDate()));
                }
                adminResponse.setCreatedBy(getAdmin.getCreatedBy());
                adminResponse.setUpdatedBy(getAdmin.getUpdatedBy());
                adminResponses.add(adminResponse);
            });
            return adminResponses;
        }
        return Collections.emptyList();
    }

    @Override
    public void deleteAdminById(Integer adminId) throws NotFoundException {
        Admin admin = adminRepository.findById(adminId)
                .orElseThrow(() -> new NotFoundException("admin not found for id : " + adminId));
        admin.setIsActive(false);
        admin.setUpdatedDate(new Date());
        adminRepository.save(admin);
    }


    private Admin buildAdminValidation(AddAdminReq addAdminReq) throws InvalidDataException {
        Admin validationMsg = new Admin();
        Admin admin = new Admin();
        if (addAdminReq.getEmailId() != null) {
            admin = adminRepository.findByEmailId(addAdminReq.getEmailId());
            if (!ObjectUtils.isEmpty(admin) && !admin.getId().equals(addAdminReq.getAdminId())) {
                throw new InvalidDataException(EMAIL_EXIST);
            }
        }
        if (addAdminReq.getContactNumber() != null) {
            admin = adminRepository.findByContactNumber(addAdminReq.getContactNumber());
            if (!ObjectUtils.isEmpty(admin) && !admin.getId().equals(addAdminReq.getAdminId())) {
                throw new InvalidDataException(CONTACT_NO_EXIST);
            }
        }
        if (addAdminReq.getUserName() != null) {
            admin = adminRepository.findByUserName(addAdminReq.getUserName());
            if (!ObjectUtils.isEmpty(admin) && !admin.getId().equals(addAdminReq.getAdminId())) {
                throw new InvalidDataException(USER_NAME_EXIST);
            }
        }
        validationMsg.setMessage(SUCCESS);
        return validationMsg;
    }
}
