package com.us.masterproject.serviceImpl;

import com.us.masterproject.exception.InvalidDataException;
import com.us.masterproject.exception.NotFoundException;
import com.us.masterproject.utils.FileStorageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class FileStorageServiceImpl {

    private final Path fileStorageLocation;
    private String filePath = "image/";

    @Autowired
    public FileStorageServiceImpl(FileStorageProperties fileStorageProperties) throws InvalidDataException {
        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir() + filePath)
                .toAbsolutePath().normalize();

        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new InvalidDataException("Could not create the directory where the uploaded files will be stored." + ex);
        }
    }


    public String storeFile(MultipartFile file) throws InvalidDataException {
        // Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            // Check if the file's name contains invalid characters
            if (fileName.contains("..")) {
                throw new InvalidDataException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = this.fileStorageLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            return fileName;
        } catch (IOException | InvalidDataException ex) {
            throw new InvalidDataException("Could not store file " + fileName + ". Please try again!" + ex);
        }
    }


    public Resource loadFileAsResource(String fileName) throws MalformedURLException, NotFoundException {

        Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
        Resource resource = new UrlResource(filePath.toUri());
        if (resource.exists()) {
            return resource;
        } else {
            throw new NotFoundException("File not found " + fileName);
        }

    }

}
