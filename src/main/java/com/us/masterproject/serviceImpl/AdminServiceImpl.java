package com.us.masterproject.serviceImpl;

import com.us.masterproject.dto.*;
import com.us.masterproject.exception.InvalidDataException;
import com.us.masterproject.exception.NotFoundException;
import com.us.masterproject.model.*;
import com.us.masterproject.repository.*;
import com.us.masterproject.service.AdminService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.us.masterproject.utils.ModelConstant.*;

@Service
@Slf4j
public class AdminServiceImpl implements AdminService {

    @Autowired
    private AdminRepository adminRepository;

    @Autowired
    private FreshClientRepository freshClientRepository;

    @Autowired
    private ExecutiveRepository executiveRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private TargetRepository targetRepository;

    @Autowired
    private AttendanceRepository attendanceRepository;

    @Autowired
    private ExcelSheetUrlRepository excelSheetUrlRepository;

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public String uploadFileFromExcel(InputStream in, Integer executiveId) throws InvalidDataException, NotFoundException {
        log.info("Enter uploadFileFromExcel() in AdminServiceImpl");
        List<FreshClient> freshClientList = new ArrayList<>();
        if (executiveId != null) {
            List<FreshClient> executiveExcel = excelToExecutiveClient(in, executiveId);
            if (!CollectionUtils.isEmpty(executiveExcel)) {
                executiveExcel.forEach(getExcelList -> {
                    FreshClient freshClient = new FreshClient();
                    freshClient.setCountryCode(getExcelList.getCountryCode());
                    freshClient.setReference(getExcelList.getReference());
                    freshClient.setClientName(getExcelList.getClientName());
                    freshClient.setContactNumber(getExcelList.getContactNumber());
                    freshClient.setStatus(getExcelList.getStatus());
                    freshClient.setIsActive(getExcelList.getIsActive());
                    freshClient.setExecutive(getExcelList.getExecutive());
                    freshClient.setCreatedBy("admin");
                    freshClient.setCreatedDate(new Date());
                    freshClientList.add(freshClient);
                });
                freshClientRepository.saveAll(freshClientList);
                log.info("End uploadFileFromExcel() in AdminServiceImpl");
                return SUCCESS;
            }
            throw new InvalidDataException("Excel sheet not uploaded");
        }
        throw new InvalidDataException(EXECUTIVE_ID_MUST);
    }

    @Override
    public Executive addUpdateExecutive(AddExecutiveReq addExecutiveReq) throws NotFoundException, InvalidDataException {
        log.info("Enter addUpdateExecutive() inside adminServiceImpl with request: {}", addExecutiveReq);
        Executive executive = new Executive();
        if (!ObjectUtils.isEmpty(addExecutiveReq)) {
            Executive validExecutiveReq = buildExecutiveValidation(addExecutiveReq);
            if (validExecutiveReq.getMessage().endsWith(SUCCESS)) {
                if (addExecutiveReq.getAdminId() != null) {
                    Executive executiveDto = buildExecutiveData(addExecutiveReq);
                    executive = executiveRepository.save(executiveDto);
                    executive.setMessage(SUCCESS);
                    return executive;
                }
                throw new InvalidDataException(ADMIN_ID_MUST);
            }
            throw new InvalidDataException(validExecutiveReq.getMessage());
        }
        throw new InvalidDataException(ADD_EXECUTIVE_REQ_MUST);
    }

    private Executive buildExecutiveValidation(AddExecutiveReq addExecutiveReq) throws InvalidDataException {
        Executive validationMsg = new Executive();
        Executive executive;
        if (addExecutiveReq.getEmailId() != null) {
            executive = executiveRepository.findByEmailId(addExecutiveReq.getEmailId());
            if (!ObjectUtils.isEmpty(executive) && !executive.getId().equals(addExecutiveReq.getExecutiveId())) {
                throw new InvalidDataException(EMAIL_EXIST);
            }
        }
        if (addExecutiveReq.getContactNumber() != null) {
            executive = executiveRepository.findByContactNumber(addExecutiveReq.getContactNumber());
            if (!ObjectUtils.isEmpty(executive) && !executive.getId().equals(addExecutiveReq.getExecutiveId())) {
                throw new InvalidDataException(CONTACT_NO_EXIST);
            }
        }
        if (addExecutiveReq.getUserName() != null) {
            executive = executiveRepository.findByUserName(addExecutiveReq.getUserName());
            if (!ObjectUtils.isEmpty(executive) && !executive.getId().equals(addExecutiveReq.getExecutiveId())) {
                throw new InvalidDataException(USER_NAME_EXIST);
            }
        }
        validationMsg.setMessage(SUCCESS);
        return validationMsg;
    }

    @Override
    public List<Executive> getAllExecutive() {
        List<Executive> allExecutiveResp = executiveRepository.findAll(Sort.by(Sort.Direction.DESC, "id"));
        if (!CollectionUtils.isEmpty(allExecutiveResp)) {
            return allExecutiveResp;
        }
        return Collections.emptyList();
    }

    @Override
    public ExecutiveResponse getExecutiveById(Integer executiveId) throws NotFoundException {
        Executive executive = executiveRepository.findByIdAndIsActive(executiveId, true)
                .orElseThrow(() -> new NotFoundException("executive not found for id : " + executiveId));
        ExecutiveResponse executiveResponse = new ExecutiveResponse();
        BeanUtils.copyProperties(executive, executiveResponse);
        executiveResponse.setExecutiveId(executive.getId());
        executiveResponse.setCreatedDate(simpleDateFormat.format(executive.getCreatedDate()));
        return executiveResponse;
    }

    @Override
    public List<ExecutiveResponse> getAllExecutiveByAdminId(Integer adminId) throws NotFoundException {
        List<ExecutiveResponse> executiveResponseList = new ArrayList<>();
        List<Executive> allExecutiveResp = executiveRepository.getAllExecutiveByAdminId(adminId);
        if (!CollectionUtils.isEmpty(allExecutiveResp)) {
            allExecutiveResp.forEach(getExecutive -> {
                ExecutiveResponse executiveResponse = new ExecutiveResponse();
                BeanUtils.copyProperties(getExecutive, executiveResponse);
                executiveResponse.setExecutiveId(getExecutive.getId());
                executiveResponse.setCreatedBy(getExecutive.getCreatedBy());
                executiveResponse.setUpdatedBy(getExecutive.getUpdatedBy());
                if (getExecutive.getCreatedDate() != null) {
                    executiveResponse.setCreatedDate(simpleDateFormat.format(getExecutive.getCreatedDate()));
                }
                if (getExecutive.getUpdatedDate() != null) {
                    executiveResponse.setUpdatedDate(simpleDateFormat.format(getExecutive.getUpdatedDate()));
                }
                executiveResponseList.add(executiveResponse);
            });
            return executiveResponseList;
        }
        throw new NotFoundException("Executive List Not Found For This Admin");
    }

    @Override
    public void deleteExecutiveById(Integer executiveId) throws NotFoundException {
        Executive executive = executiveRepository.findById(executiveId)
                .orElseThrow(() -> new NotFoundException("executive not found for id : " + executiveId));
        executive.setIsActive(false);
        executive.setUpdatedDate(new Date());
        executiveRepository.save(executive);
    }

    @Override
    public Target addUpdateTarget(AddTargetReq addTargetReq) throws NotFoundException, InvalidDataException {
        log.info("Enter addUpdateTarget() inside adminServiceImpl with request: {}", addTargetReq);
        Target target = new Target();
        Target savedTarget = null;
        if (!ObjectUtils.isEmpty(addTargetReq)) {
            if (addTargetReq.getExecutiveId() != null) {
                Executive executive = executiveRepository.findById(addTargetReq.getExecutiveId())
                        .orElseThrow(() -> new NotFoundException("executive not found for id : " + addTargetReq.getExecutiveId()));
                if (addTargetReq.getTargetId() != null) {
                    Target getTarget = targetRepository.findById(addTargetReq.getTargetId())
                            .orElseThrow(() -> new NotFoundException("target details not found form id : " + addTargetReq.getTargetId()));
                    BeanUtils.copyProperties(addTargetReq, getTarget);
                    getTarget.setExecutive(executive);
                    getTarget.setUpdatedBy("admin");
                    getTarget.setUpdatedDate(new Date());
                    savedTarget = targetRepository.save(getTarget);
                    savedTarget.setMessage(SUCCESS);
                    return savedTarget;
                }
                if (Boolean.FALSE.equals(targetRepository.existsByExecutiveIdAndTargetDate(addTargetReq.getExecutiveId(), addTargetReq.getTargetDate()))) {
                    BeanUtils.copyProperties(addTargetReq, target);
                    target.setExecutive(executive);
                    target.setCreatedBy("admin");
                    target.setCreatedDate(new Date());
                    savedTarget = targetRepository.save(target);
                    savedTarget.setMessage(SUCCESS);
                    return savedTarget;
                }
                throw new InvalidDataException(TARGET_ALREADY_EXIST);
            }
            throw new InvalidDataException(EXECUTIVE_ID_MUST);
        }
        throw new InvalidDataException(EOD_REQ_MUST);
    }

    @Override
    public List<TargetResponse> getAllTargetByExecutiveId(Integer executiveId) throws NotFoundException {
        List<TargetResponse> targetResponseArrayList = new ArrayList<>();
        List<Target> allTargetResp = targetRepository.findAllByExecutiveId(executiveId);
        if (!CollectionUtils.isEmpty(allTargetResp)) {
            allTargetResp.forEach(getTarget -> {
                TargetResponse targetResponse = new TargetResponse();
                if (getTarget.getExecutive() != null && getTarget.getExecutive().getId() != null) {
                    Optional<Executive> executive = executiveRepository.findById(getTarget.getExecutive().getId());
                    if (executive.isPresent() && executive.get().getFirstName() != null && executive.get().getLastName() != null) {
                        targetResponse.setExecutiveName(executive.get().getFirstName() + " " + executive.get().getLastName());
                        targetResponse.setExecutiveId(executive.get().getId());
                    }
                }
                BeanUtils.copyProperties(getTarget, targetResponse);
                targetResponse.setTargetId(getTarget.getId());
                targetResponse.setCreatedBy(getTarget.getCreatedBy());
                targetResponse.setUpdatedBy(getTarget.getUpdatedBy());
                if (getTarget.getCreatedDate() != null) {
                    targetResponse.setCreatedDate(simpleDateFormat.format(getTarget.getCreatedDate()));
                }
                if (getTarget.getUpdatedDate() != null) {
                    targetResponse.setUpdatedDate(simpleDateFormat.format(getTarget.getUpdatedDate()));
                }
                targetResponseArrayList.add(targetResponse);
            });
            return targetResponseArrayList;
        }
        throw new NotFoundException("Target Not Found For This Executive");
    }

    @Override
    public List<AttendanceResponse> getAllAttendanceByExecutiveId(Integer executiveId, Integer month, Integer year) throws NotFoundException, InvalidDataException {
        List<AttendanceResponse> attendanceArrayList = new ArrayList<>();
        Executive executive;
        if (executiveId != null) {
            executive = executiveRepository.findById(executiveId).orElseThrow(() -> new NotFoundException("executive not for this id: " + executiveId));
            List<Attendance> attendanceList = attendanceRepository.getAllAttendanceByExecutiveId(executiveId, month, year);
            if (!CollectionUtils.isEmpty(attendanceList)) {
                attendanceList.forEach(getAttendance -> {
                    AttendanceResponse attendanceResponse = new AttendanceResponse();
                    BeanUtils.copyProperties(getAttendance, attendanceResponse);
                    attendanceResponse.setAttendanceId(getAttendance.getId());
                    attendanceResponse.setExecutiveName(executive.getFirstName() + " " + executive.getLastName());
                    if (getAttendance.getCreatedDate() != null) {
                        attendanceResponse.setCreatedDate(simpleDateFormat.format(getAttendance.getCreatedDate()));
                    }
                    if (getAttendance.getUpdatedDate() != null) {
                        attendanceResponse.setUpdatedDate(simpleDateFormat.format(getAttendance.getUpdatedDate()));
                    }
                    attendanceArrayList.add(attendanceResponse);
                });
                return attendanceArrayList;
            }
            throw new NotFoundException("Attendance details not for this executive");
        }
        throw new InvalidDataException(EXECUTIVE_ID_MUST);
    }

    @Override
    public TargetResponse getTargetByTargetId(Integer targetId) throws NotFoundException {
        TargetResponse targetResponse = new TargetResponse();
        Target allTargetResp = targetRepository.findById(targetId).orElseThrow(() -> new NotFoundException("Target not found "));
        if (allTargetResp != null) {
            Executive executive = executiveRepository.findById(allTargetResp.getExecutive().getId()).orElseThrow(() -> new NotFoundException("executive not found"));
            BeanUtils.copyProperties(allTargetResp, targetResponse);
            targetResponse.setTargetId(allTargetResp.getId());
            targetResponse.setCreatedBy(allTargetResp.getCreatedBy());
            targetResponse.setUpdatedBy(allTargetResp.getUpdatedBy());
            if (executive != null && executive.getFirstName() != null && executive.getLastName() != null) {
                targetResponse.setExecutiveName(executive.getFirstName() + " " + executive.getLastName());
                targetResponse.setExecutiveId(executive.getId());
            }
            if (allTargetResp.getCreatedDate() != null) {
                targetResponse.setCreatedDate(simpleDateFormat.format(allTargetResp.getCreatedDate()));
            }
            if (allTargetResp.getUpdatedDate() != null) {
                targetResponse.setUpdatedDate(simpleDateFormat.format(allTargetResp.getUpdatedDate()));
            }
            return targetResponse;
        }
        throw new NotFoundException("target not found");
    }

    @Override
    public ExcelSheetUrl addUpdateExcelSheetUrl(ExcelSheetUrlDto excelSheetUrlDto) throws NotFoundException, InvalidDataException {
        log.info("Enter addUpdateExcelSheetUrl() inside adminServiceImpl with request: {}", excelSheetUrlDto);
        ExcelSheetUrl excelSheetUrl = new ExcelSheetUrl();
        ExcelSheetUrl savedExcelSheetUrl;
        if (!ObjectUtils.isEmpty(excelSheetUrlDto)) {
            if (!StringUtils.isEmpty(excelSheetUrlDto.getExcelSheetUrl())) {
                Admin adminFromDb = adminRepository.findById(excelSheetUrlDto.getAdminId())
                        .orElseThrow(() -> new NotFoundException("admin not found"));
                if (excelSheetUrlDto.getId() != null) {
                    ExcelSheetUrl getExcelSheetUrl = excelSheetUrlRepository.findById(excelSheetUrlDto.getId())
                            .orElseThrow(() -> new NotFoundException("excelSheet url not found"));
                    getExcelSheetUrl.setExcelSheetTitle(excelSheetUrlDto.getExcelSheetTitle()!=null ? excelSheetUrlDto.getExcelSheetTitle():null);
                    getExcelSheetUrl.setExcelSheetUrl(excelSheetUrlDto.getExcelSheetUrl() != null ? excelSheetUrlDto.getExcelSheetUrl() : null);
                    getExcelSheetUrl.setAdmin(adminFromDb);
                    getExcelSheetUrl.setUpdatedBy("admin");
                    getExcelSheetUrl.setUpdatedDate(new Date());
                    savedExcelSheetUrl = excelSheetUrlRepository.save(getExcelSheetUrl);
                    savedExcelSheetUrl.setMessage(SUCCESS);
                    return savedExcelSheetUrl;
                }
                excelSheetUrl.setExcelSheetTitle(excelSheetUrlDto.getExcelSheetTitle()!=null ? excelSheetUrlDto.getExcelSheetTitle():null);
                excelSheetUrl.setExcelSheetUrl(excelSheetUrlDto.getExcelSheetUrl() != null ? excelSheetUrlDto.getExcelSheetUrl() : null);
                excelSheetUrl.setAdmin(adminFromDb);
                excelSheetUrl.setCreatedBy("admin");
                excelSheetUrl.setCreatedDate(new Date());
                savedExcelSheetUrl = excelSheetUrlRepository.save(excelSheetUrl);
                savedExcelSheetUrl.setMessage(SUCCESS);
                return savedExcelSheetUrl;
            }
            throw new InvalidDataException(EXCEL_SHEET_URL_MUST);
        }
        throw new InvalidDataException(EXCEL_SHEET_REQ_MUST);
    }

    @Override
    public List<ExcelSheetUrlDto> getAllExcelSheetUrlByAdminId(Integer adminId) throws NotFoundException, InvalidDataException {
        List<ExcelSheetUrlDto> excelSheetUrlDtoArrayList = new ArrayList<>();
        if (adminId != null) {
            List<ExcelSheetUrl> excelSheetUrls = excelSheetUrlRepository.getAllExcelSheetUrlByAdminId(adminId);
            if (!CollectionUtils.isEmpty(excelSheetUrls)) {
                excelSheetUrls.forEach(getExcelSheetUrls -> {
                    ExcelSheetUrlDto excelSheetUrlDto = new ExcelSheetUrlDto();
                    excelSheetUrlDto.setId(getExcelSheetUrls.getId());
                    excelSheetUrlDto.setExcelSheetTitle(getExcelSheetUrls.getExcelSheetTitle());
                    excelSheetUrlDto.setExcelSheetUrl(getExcelSheetUrls.getExcelSheetUrl());
                    excelSheetUrlDto.setAdminId(getExcelSheetUrls.getAdmin().getId());
                    excelSheetUrlDtoArrayList.add(excelSheetUrlDto);
                });
                return excelSheetUrlDtoArrayList;
            }
            throw new NotFoundException("excelSheet urls not found this admin");
        }
        throw new InvalidDataException(ADMIN_ID_MUST);
    }

    @Override
    public void deleteExcelSheetUrlById(Integer excelSheetUrlId) throws NotFoundException {
        ExcelSheetUrl excelSheetUrl = excelSheetUrlRepository.findById(excelSheetUrlId)
                .orElseThrow(() -> new NotFoundException("excelSheet url not found"));
        excelSheetUrlRepository.delete(excelSheetUrl);
    }


    private Executive buildExecutiveData(AddExecutiveReq addExecutiveReq) throws NotFoundException {
        Admin admin = adminRepository.findById(addExecutiveReq.getAdminId())
                .orElseThrow(() -> new NotFoundException("admin not found for id : " + addExecutiveReq.getAdminId()));
        if (addExecutiveReq.getExecutiveId() != null) {
            Executive executive = executiveRepository.findById(addExecutiveReq.getExecutiveId())
                    .orElseThrow(() -> new NotFoundException("executive not found for id : " + addExecutiveReq.getExecutiveId()));
            if (!ObjectUtils.isEmpty(executive)) {
                addExecutiveReq.setPassword(executive.getPassword() != null ? executive.getPassword() : null);
                BeanUtils.copyProperties(addExecutiveReq, executive);
                executive.setAdmin(admin);
                executive.setUpdatedBy("Admin");
                executive.setUpdatedDate(new Date());
                return executive;
            }
        }
        Role role = roleRepository.findById(3).orElseThrow(() -> new NotFoundException("Role not found for id : " + 3));
        return Executive.builder().admin(admin)
                .firstName(addExecutiveReq.getFirstName())
                .lastName(addExecutiveReq.getLastName())
                .userName(addExecutiveReq.getUserName())
                .contactNumber(addExecutiveReq.getContactNumber())
                .emailId(addExecutiveReq.getEmailId())
                .password(Base64.getEncoder().encodeToString(addExecutiveReq.getPassword().getBytes()))
                .remark(addExecutiveReq.getRemark())
                .role(role)
                .isActive(true)
                .createdBy("").createdDate(new Date())
                .build();
    }

    private List<FreshClient> excelToExecutiveClient(InputStream in, Integer executiveId) throws NotFoundException, InvalidDataException {
        log.info("Enter excelToExecutiveClient() in AdminServiceImpl");
        List<FreshClient> executiveExcelList = new ArrayList<FreshClient>();
        if (executiveId != null) {
            Executive executive = executiveRepository.findById(executiveId)
                    .orElseThrow(() -> new NotFoundException("executive not found for id : " + executiveId));
            if (!ObjectUtils.isEmpty(executive)) {
                try {
                    Workbook workbook = new XSSFWorkbook(in);

                    Sheet sheet = workbook.getSheetAt(0);
                    Iterator<Row> rows = sheet.iterator();

                    int rowNumber = 0;
                    while (rows.hasNext()) {
                        Row currentRow = rows.next();

                        // skip header
                        if (rowNumber == 0) {
                            rowNumber++;
                            continue;
                        }
                        FreshClient executiveExcel;
                        List<Cell> cells = new ArrayList<>();

                        int lastColumn = Math.max(currentRow.getLastCellNum(), 4);
                        for (int cn = 0; cn < lastColumn; cn++) {
                            Cell c = currentRow.getCell(cn, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL);
                            cells.add(c);
                        }
                        executiveExcel = extractInfoFromCell(cells);
                        executiveExcel.setExecutive(executive);
                        executiveExcel.setStatus(FRESH_CLIENT);
                        executiveExcel.setIsActive(true);
                        executiveExcelList.add(executiveExcel);
                        log.info("executiveExcelList size:{}", executiveExcelList.size());
                    }

                    workbook.close();
                    log.info("End excelToExecutiveClient() in AdminServiceImpl");
                } catch (IOException e) {
                    throw new InvalidDataException("fail to parse Excel file: " + e.getMessage());
                }
            }
        }
        return executiveExcelList;
    }

    private static FreshClient extractInfoFromCell(List<Cell> cells) {
        FreshClient freshClient = new FreshClient();

        Cell nameCell = cells.get(0);
        if (nameCell != null) {
            freshClient.setClientName(nameCell.getStringCellValue());
        }

        Cell countryCodeCell = cells.get(1);
        if (countryCodeCell != null) {
            if (countryCodeCell.getCellType() == CellType.NUMERIC) {
                freshClient.setCountryCode(NumberToTextConverter.toText(countryCodeCell.getNumericCellValue()));
            }
        }

        Cell phoneCell = cells.get(2);
        if (phoneCell != null) {
            if (phoneCell.getCellType() == CellType.NUMERIC) {
                freshClient.setContactNumber(NumberToTextConverter.toText(phoneCell.getNumericCellValue()));
            }
        }

        Cell permAddressCell = cells.get(3);
        if (permAddressCell != null) {
            freshClient.setReference(permAddressCell.getStringCellValue());
        }

        return freshClient;
    }

}

