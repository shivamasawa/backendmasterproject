package com.us.masterproject.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "excel_sheet_url")
public class ExcelSheetUrl {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "excel_sheet_url")
    private String excelSheetUrl;

    @Column(name = "excel_sheet_title")
    private String excelSheetTitle;

    @JoinColumn(name = "admin")
    @ManyToOne
    private Admin admin;

    @Column(name = "created_by")
    private String createdBy;

    @Temporal(value=TemporalType.DATE)
    @Column(name = "created_date")
    private Date createdDate;

    @Column(name = "updated_by")
    private String updatedBy;

    @Temporal(value=TemporalType.DATE)
    @Column(name = "updated_date")
    private Date updatedDate;

    @Transient
    private String message;


}
