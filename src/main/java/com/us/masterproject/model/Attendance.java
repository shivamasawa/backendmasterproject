package com.us.masterproject.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "attendance")
public class Attendance {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id")
        private Integer id;

        @Column(name = "login_time")
        private String logInTime;

        @Column(name = "logout_time")
        private String logOutTime;

        @JoinColumn(name = "executive_id")
        @ManyToOne
        private Executive executive;

        @Column(name = "created_by")
        private String createdBy;

        @Temporal(value=TemporalType.DATE)
        @Column(name = "created_date")
        private Date createdDate;

        @Column(name = "updated_by")
        private String updatedBy;

        @Temporal(value=TemporalType.DATE)
        @Column(name = "updated_date")
        private Date updatedDate;

        @Transient
        private String message;

}
