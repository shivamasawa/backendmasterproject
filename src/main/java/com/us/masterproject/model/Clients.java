package com.us.masterproject.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;


@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "client")
public class Clients {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "client_name")
    private String clientName;

    @Column(name = "contact_number")
    private String contactNumber;

    @Column(name = "university_name")
    private String universityName;

    @Column(name = "subject")
    private String subject;

    @Column(name = "country_code")
    private String countryCode;

    @Column(name = "reference")
    private String reference;

    @JoinColumn(name = "executive_id")
    @ManyToOne(cascade = {CascadeType.MERGE})
    private Executive executive;

    @Temporal(value = TemporalType.DATE)
    @Column(name = "next_followup_date")
    private Date nextFollowupDate;

    @Column(name = "created_by")
    private String createdBy;

    @Temporal(value = TemporalType.DATE)
    @Column(name = "created_date")
    private Date createdDate;

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "updated_date")
    private Date updatedDate;

    @Temporal(value = TemporalType.DATE)
    @Column(name = "date_of_birth")
    private Date dateOfBirth;

    @Column(name = "remark")
    private String remark;

    @Column(name = "email")
    private String email;

    @Column(name = "other_details")
    private String otherDetails;

    @Column(name = "status")
    private String status;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "fresh_client_id")
    private Integer freshClientId;

    @Transient
    private String message;

}
