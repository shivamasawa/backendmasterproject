package com.us.masterproject.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "fresh_client")
public class FreshClient {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "client_name")
    private String clientName;

    @Column(name = "contact_number")
    private String contactNumber;

    @Column(name = "country_code")
    private String countryCode;

    @Column(name = "reference")
    private String reference;

    @Column(name = "status")
    private String status;

    @Column(name = "is_active")
    private Boolean isActive;

    @JoinColumn(name = "executive_id")
    @ManyToOne
    private Executive executive;

    @Column(name = "created_by")
    private String createdBy;

    @Temporal(value = TemporalType.DATE)
    @Column(name = "created_date")
    private Date createdDate;

    @Column(name = "updated_by")
    private String updatedBy;

    @Temporal(value = TemporalType.DATE)
    @Column(name = "updated_date")
    private Date updatedDate;

    @Transient
    private String message;

}
