package com.us.masterproject.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "eod")
public class EodForm {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "pending_client_added")
    private Integer pendingClientAdded;

    @Column(name = "converted_client_added")
    private Integer convertedClientAdded;

    @Column(name = "pending_task_clear")
    private Integer pendingTaskClear;

    @Column(name = "remark")
    private String remark;

    @Column(name = "log_out_time")
    private String logOutTime;

    @Column(name = "login_time")
    private String logInTime;

    @JoinColumn(name = "executive_id")
    @ManyToOne
    private Executive executive;

    @Column(name = "created_by")
    private String createdBy;

    @Temporal(value=TemporalType.DATE)
    @Column(name = "eod_date")
    private Date eodDate;

    @Temporal(value=TemporalType.DATE)
    @Column(name = "created_date")
    private Date createdDate;

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "updated_date")
    private Date updatedDate;

    @Transient
    private String message;
}
