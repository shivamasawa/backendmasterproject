package com.us.masterproject.model;

import java.util.Date;

import javax.persistence.*;

import lombok.Data;

@Data
@Entity
@Table(name = "installment")
public class Installment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "installment_amount")
    private String installmentAmount;

    @Column(name = "transaction_id")
    private String transactionId;

    @JoinColumn(name = "converted_client_id")
    @ManyToOne
    private ConvertedClient convertedClient;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_date")
    private Date createdDate;

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "updated_date")
    private Date updatedDate;

    @Transient
    private String message;
}
