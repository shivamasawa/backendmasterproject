package com.us.masterproject.model;

import java.util.Date;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "converted_client")
public class ConvertedClient {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "client_name")
    private String clientName;

    @Column(name = "contact_number")
    private String contactNumber;

    @Column(name = "university_name")
    private String universityName;

    @Column(name = "subject")
    private String subject;

    @Temporal(value=TemporalType.DATE)
    @Column(name = "next_followup_date")
    private Date nextFollowupDate;

    @Temporal(value=TemporalType.DATE)
    @Column(name = "date_of_birth")
    private Date dateOfBirth;

    @Column(name = "remark")
    private String remark;

    @Column(name = "other_details")
    private String otherDetails;

    @Column(name = "topic")
    private String topic;

    @Column(name = "word_count")
    private String wordCount;

    @Temporal(value=TemporalType.DATE)
    @Column(name = "due_date")
    private Date dueDate;

    @Column(name = "no_of_files")
    private String noOfFile;

    @Column(name = "total_amount")
    private String totalAmount;

    @Column(name = "remaining_amount")
    private String remainingAmount;

    @Column(name = "code_secret")
    private String codeSecret;

    @Column(name = "services")
    private String services;

    @Column(name = "email")
    private String email;

    @Column(name = "country_code")
    private String countryCode;

    @Column(name = "reference")
    private String reference;

    @Column(name = "status")
    private String status;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "is_completed")
    private Boolean isCompleted;

    @JoinColumn(name = "executive_id")
    @ManyToOne
    private Executive executive;

    @Column(name = "created_by")
    private String createdBy;

    @Temporal(value = TemporalType.DATE)
    @Column(name = "created_date")
    private Date createdDate;

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "updated_date")
    private Date updatedDate;

    @Transient
    private String message;
}
