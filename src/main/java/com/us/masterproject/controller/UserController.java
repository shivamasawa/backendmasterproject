package com.us.masterproject.controller;

import com.us.masterproject.dto.LoginReq;
import com.us.masterproject.dto.UserResponse;
import com.us.masterproject.exception.InvalidDataException;
import com.us.masterproject.exception.NotFoundException;
import com.us.masterproject.handler.GenericResponseHandler;
import com.us.masterproject.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;

import static com.us.masterproject.utils.ModelConstant.SUCCESS;

@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/loginUser")
    public ResponseEntity<?> loginUser(@RequestBody final LoginReq loginReq)
            throws InvalidDataException, NotFoundException {
        UserResponse user = userService.loginUser(loginReq.getRoleId(), loginReq.getUserName(), loginReq.getPassword(), loginReq.getLogInTime());
        return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK).setMessage("Login Success").setData(user).create();

    }

    @PutMapping("/updatePassword")
    public ResponseEntity<?> updatePassword(@RequestParam Integer id,@RequestParam Integer roleId,@RequestParam String password) throws NotFoundException, ParseException, InvalidDataException {
        String responseMsg = userService.updatePassword(id,roleId,password);
        if (responseMsg.endsWith(SUCCESS)) {
            return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK).setMessage("password update successfully")
                    .create();
        } else {
            return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK).setMessage(responseMsg)
                    .create();
        }
    }
}


