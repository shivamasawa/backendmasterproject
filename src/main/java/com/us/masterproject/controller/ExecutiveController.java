package com.us.masterproject.controller;

import com.us.masterproject.dto.*;
import com.us.masterproject.exception.InvalidDataException;
import com.us.masterproject.exception.NotFoundException;
import com.us.masterproject.handler.GenericResponseHandler;
import com.us.masterproject.model.EodForm;
import com.us.masterproject.model.FreshClient;
import com.us.masterproject.service.ClientService;
import com.us.masterproject.service.ExecutiveService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

import static com.us.masterproject.utils.ModelConstant.SUCCESS;

@Slf4j
@RestController
@RequestMapping("/executive")
public class ExecutiveController {

    @Autowired
    private ExecutiveService executiveService;

    @Autowired
    private ClientService clientService;

    @GetMapping("/getAllFreshClient")
    public ResponseEntity<?> getAllFreshClient() {
        List<FreshClient> freshClientsResponseList = executiveService.getAllFreshClientResponseList();
        return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                .setMessage("All fresh clients fetched successfully").setData(freshClientsResponseList).create();
    }

    @GetMapping("/freshClient/{freshClientId}")
    public ResponseEntity<?> getFreshClientById(Integer freshClientId) throws NotFoundException {
        FreshClientResponse response = executiveService.getFreshClientById(freshClientId);
        if (!ObjectUtils.isEmpty(response)) {
            return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                    .setMessage("Fresh client fetched successfully").setData(response).create();
        } else {
            return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.NOT_FOUND)
                    .setMessage("Fresh client not found").setData(response).create();
        }
    }

    @GetMapping("/getAllFreshClientByExecutiveId/{executiveId}")
    public ResponseEntity<?> getAllFreshClientByExecutiveId(Integer executiveId) throws InvalidDataException {
        List<FreshClientResponse> freshClientsResponseList = executiveService.getAllFreshClientByExecutiveId(executiveId);
        if (freshClientsResponseList.isEmpty()) {
            return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                    .setMessage("fresh clients not found").setData(freshClientsResponseList).create();
        }
        return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                .setMessage("All fresh clients fetched successfully").setData(freshClientsResponseList).create();
    }

    @PostMapping("/deleteFreshClientByIds")
    public ResponseEntity<?> deleteFreshClientById(@RequestBody SoftDeleteDto softDeleteDto) throws InvalidDataException {
        clientService.deleteFreshClientById(softDeleteDto);
        return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                .setMessage("fresh client delete successfully").create();
    }

    @PostMapping("/addUpdateEodForm")
    public ResponseEntity<?> addUpdateEodForm(AddEodFormReq addEodFormReq) throws NotFoundException, InvalidDataException {
        log.info("Enter addUpdateEodForm() inside executive controller with request body: {}", addEodFormReq);
        EodForm eodForm = executiveService.addUpdateEodForm(addEodFormReq);
        if (eodForm.getMessage().endsWith(SUCCESS)) {
            log.info("Enter addUpdateEodForm() inside executive controller with msg: {}", eodForm.getMessage());
            return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK).setMessage("Submit Eod Form Successfully")
                    .create();
        }
        log.info("End addUpdateEodForm() inside executive controller with msg: {}", eodForm.getMessage());
        return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK).setMessage(eodForm.getMessage())
                .create();
    }

    @GetMapping("/getAllEodFormDetailsByExecutiveId/{executiveId}")
    public ResponseEntity<?> getAllEodFormDetailsByExecutiveId(Integer executiveId) throws InvalidDataException, NotFoundException {
        List<EodFormResponse> eodFormResponseList = executiveService.getAllEodFormDetailsByExecutiveId(executiveId);
        if (eodFormResponseList.isEmpty()) {
            return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                    .setMessage("eod form details  not found").setData(eodFormResponseList).create();
        }
        return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                .setMessage("All eod form details fetched successfully").setData(eodFormResponseList).create();
    }

    @GetMapping("/getEodFormDetailsByDate")
    public ResponseEntity<?> getEodFormDetailsByDate(@RequestParam("executiveId") Integer executiveId, @RequestParam("todayDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date todayDate) throws NotFoundException, InvalidDataException {
        Boolean response = executiveService.getEodFormDetailsByDate(executiveId, todayDate);
        if (Boolean.FALSE.equals(response)) {
            return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                    .setMessage("Today eod form need to submit").setData(false).create();
        }
        return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                .setMessage("Today eod form submitted already").setData(response).create();
    }

    @PostMapping("/addUpdateFreshClient")
    public ResponseEntity<?> addUpdateFreshClient(AddFreshClient addFreshClient) throws NotFoundException, InvalidDataException {
        log.info("Enter addUpdateFreshClient() inside executive controller with request body: {}", addFreshClient);
        FreshClient freshClient = executiveService.addUpdateFreshClient(addFreshClient);
        if (freshClient.getMessage().endsWith(SUCCESS)) {
            log.info("Enter addUpdateFreshClient() inside executive controller with msg: {}", freshClient.getMessage());
            return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK).setMessage("Submit Eod Form Successfully")
                    .create();
        }
        log.info("End addUpdateFreshClient() inside executive controller with msg: {}", freshClient.getMessage());
        return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK).setMessage(freshClient.getMessage())
                .create();
    }
}
