package com.us.masterproject.controller;

import com.us.masterproject.dto.AddAdminReq;
import com.us.masterproject.dto.AdminResponse;
import com.us.masterproject.exception.InvalidDataException;
import com.us.masterproject.exception.NotFoundException;
import com.us.masterproject.handler.GenericResponseHandler;
import com.us.masterproject.model.Admin;
import com.us.masterproject.service.SuperAdminService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.us.masterproject.utils.ModelConstant.SUCCESS;

@Slf4j
@RestController
@RequestMapping("/superAdmin")
public class SuperAdminController {

    @Autowired
    private SuperAdminService superAdminService;

    @PostMapping("/addUpdateAdmin")
    public ResponseEntity<?> addUpdateAdmin(AddAdminReq addAdminReq) throws NotFoundException, InvalidDataException {
        log.info("Enter addUpdateAdmin() inside admin controller with request body: {}", addAdminReq);
        Admin admin = superAdminService.addUpdateAdmin(addAdminReq);
        if (admin.getMessage().endsWith(SUCCESS)) {
            log.info("Enter addUpdateAdmin() inside admin controller with msg: {}", admin.getMessage());
            return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK).setMessage("admin added successfully")
                    .create();
        }
        log.info("End addUpdateAdmin() inside admin controller with msg: {}", admin.getMessage());
        return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK).setMessage(admin.getMessage())
                .create();
    }

    @GetMapping("/getAllAdmin")
    public ResponseEntity<?> getAllAdmin() {
        List<AdminResponse> adminResponseList = superAdminService.getAllAdmin();
        if (CollectionUtils.isEmpty(adminResponseList)) {
            return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                    .setMessage("admin not found").setData(adminResponseList).create();
        }
        return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                .setMessage("All admin fetched successfully").setData(adminResponseList).create();
    }

    @GetMapping("/getAdminById/{adminId}")
    public ResponseEntity<?> getAdminById(Integer adminId) throws NotFoundException {
        AdminResponse adminResponse = superAdminService.getAdminById(adminId);
        return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                .setMessage("admin fetched successfully").setData(adminResponse).create();
    }

    @PutMapping("/deleteAdminById/{adminId}")
    public ResponseEntity<?> deleteAdminById(Integer adminId) throws NotFoundException {
        superAdminService.deleteAdminById(adminId);
        return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                .setMessage("admin delete successfully").create();
    }
}
