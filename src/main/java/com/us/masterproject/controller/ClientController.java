package com.us.masterproject.controller;

import com.us.masterproject.dto.*;
import com.us.masterproject.exception.InvalidDataException;
import com.us.masterproject.exception.NotFoundException;
import com.us.masterproject.handler.GenericResponseHandler;
import com.us.masterproject.model.Clients;
import com.us.masterproject.model.ConvertedClient;
import com.us.masterproject.model.Installment;
import com.us.masterproject.service.ClientService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import static com.us.masterproject.utils.ModelConstant.SUCCESS;

@Slf4j
@RestController
@RequestMapping("/client")
public class ClientController {

    @Autowired
    private ClientService clientService;

    @PostMapping("/addUpdateClient")
    public ResponseEntity<?> addUpdateClient(AddClientReq addClientReq) throws NotFoundException, ParseException, InvalidDataException {
        Clients clients = clientService.saveOrUpdateClient(addClientReq);
        if (clients.getMessage().endsWith(SUCCESS)) {
            return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK).setMessage("client added successfully")
                    .create();
        } else {
            return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK).setMessage(clients.getMessage())
                    .create();
        }
    }

    @GetMapping("/getAllClient")
    public ResponseEntity<?> getAllClient() {
        List<ClientsResponse> clientsResponseList = clientService.getAllClientsResponseList();
        return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                .setMessage("All Clients fetched successfully").setData(clientsResponseList).create();
    }

    @GetMapping("/getAllPendingClientByDate")
    public ResponseEntity<?> getAllPendingClientByDate(@RequestParam("executiveId") Integer executiveId, @RequestParam("followUpDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date followUpDate) throws ParseException, NotFoundException {
        List<ClientsResponse> pendingClientsResponseList = clientService.getAllPendingClientByDate(executiveId, followUpDate);
        if (!pendingClientsResponseList.isEmpty()) {
            return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                    .setMessage("All pending clients fetched successfully").setData(pendingClientsResponseList).create();
        }
        return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                .setMessage("pending clients not found").setData(pendingClientsResponseList).create();
    }

    @PostMapping("/addUpdateConvertedClient")
    public ResponseEntity<?> addUpdateConvertedClient(ConvertedClientReq convertedClientReq) throws NotFoundException, ParseException, InvalidDataException {
        ConvertedClient convertedClient = clientService.saveConvertedClient(convertedClientReq);
        if (convertedClient.getMessage().endsWith(SUCCESS)) {
            return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK).setMessage("converted client added successfully")
                    .create();
        } else {
            return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setMessage(convertedClient.getMessage())
                    .create();
        }
    }

    @GetMapping("/getAllConvertedClientByDate")
    public ResponseEntity<?> getAllConvertedClientByDate(@RequestParam("executiveId") Integer executiveId, @RequestParam("followUpDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date followUpDate) throws ParseException, NotFoundException {
        List<ConvertedClientResponse> convertedClientResponseList = clientService.getAllConvertedClientByDate(executiveId, followUpDate);
        if (convertedClientResponseList.isEmpty()) {
            return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                    .setMessage("converted clients not found").setData(convertedClientResponseList).create();
        }
        return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                .setMessage("All converted clients fetched successfully").setData(convertedClientResponseList).create();
    }

    @GetMapping("/getAllConvertedClientByExecutiveId")
    public ResponseEntity<?> getAllConvertedClientByExecutiveId(@RequestParam("executiveId") Integer executiveId) throws ParseException, NotFoundException {
        List<ConvertedClientResponse> convertedClientResponseList = clientService.getAllConvertedClientByExecutiveId(executiveId);
        if (convertedClientResponseList.isEmpty()) {
            return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                    .setMessage("converted clients not found").setData(convertedClientResponseList).create();
        }
        return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                .setMessage("All converted clients fetched successfully").setData(convertedClientResponseList).create();
    }

    @GetMapping("/getAllPendingClientByExecutiveId")
    public ResponseEntity<?> getAllPendingClientByExecutiveId(@RequestParam("executiveId") Integer executiveId) throws ParseException, NotFoundException {
        List<ClientsResponse> pendingClientsResponseList = clientService.getAllPendingClientByExecutiveId(executiveId);
        if (!pendingClientsResponseList.isEmpty()) {
            return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                    .setMessage("All pending clients fetched successfully").setData(pendingClientsResponseList).create();
        }
        return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                .setMessage("pending clients not found").setData(pendingClientsResponseList).create();
    }

    @GetMapping("/getPendingClientById/{pendingClientId}")
    public ResponseEntity<?> getPendingClientById(Integer pendingClientId) throws NotFoundException {
        ClientsResponse response = clientService.getPendingClientById(pendingClientId);
        if (response != null) {
            return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                    .setMessage("pending client fetched successfully").setData(response).create();
        }
        return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                .setMessage("pending client not found").setData(null).create();
    }

    @GetMapping("/getConvertedClientById/{convertedClientId}")
    public ResponseEntity<?> getConvertedClientById(Integer convertedClientId) throws NotFoundException {
        ConvertedClientResponse response = clientService.getConvertedClientById(convertedClientId);
        if (ObjectUtils.isEmpty(response)) {
            return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                    .setMessage("converted client not found").setData(response).create();
        }
        return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                .setMessage("converted client fetched successfully").setData(response).create();
    }

    @PutMapping("/updateInstallment")
    public ResponseEntity<?> updateInstallment(InstallmentReq installmentReq) throws NotFoundException, ParseException, InvalidDataException {
        Installment installment = clientService.updateInstallment(installmentReq);
        if (installment.getMessage().endsWith(SUCCESS)) {
            return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK).setMessage("installment update successfully")
                    .create();
        } else {
            return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK).setMessage(installment.getMessage())
                    .create();
        }
    }

    @GetMapping("/getTargetByDate")
    public ResponseEntity<?> getTargetByDate(@RequestParam("executiveId") Integer executiveId, @RequestParam("targetDate") String targetDate) throws NotFoundException {
        TargetResponse targetResponses = clientService.getTargetByDate(executiveId, targetDate);
        if (ObjectUtils.isEmpty(targetResponses)) {
            return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                    .setMessage("Target details not found").setData(targetResponses).create();
        }
        return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                .setMessage("Target  details fetched successfully").setData(targetResponses).create();
    }

    @GetMapping("/getConvertedClientCountByExecutiveId")
    public ResponseEntity<?> getConvertedClientCountByExecutiveId(@RequestParam("executiveId") Integer executiveId, @RequestParam("month") Integer month, @RequestParam("year") Integer year) throws ParseException {
        Integer count = clientService.getConvertedClientCountByExecutiveId(executiveId, month, year);
        if (count != null) {
            return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                    .setMessage("converted clients count").setData(count).create();
        }
        return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                .setMessage("not converted any clients").setData(count).create();
    }

    @PutMapping("/changeCompletedStatusByConvertedClientId/{convertedClientId}")
    public ResponseEntity<?> changeCompletedStatusByConvertedClientId(Integer convertedClientId,Boolean status) throws NotFoundException {
        clientService.changeCompletedStatusByConvertedClientId(convertedClientId,status);
        return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                .setMessage("converted client completed successfully").create();
    }
}
