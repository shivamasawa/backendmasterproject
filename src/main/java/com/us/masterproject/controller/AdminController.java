package com.us.masterproject.controller;

import com.us.masterproject.dto.*;
import com.us.masterproject.exception.InvalidDataException;
import com.us.masterproject.exception.NotFoundException;
import com.us.masterproject.handler.GenericResponseHandler;
import com.us.masterproject.model.EodForm;
import com.us.masterproject.model.ExcelSheetUrl;
import com.us.masterproject.model.Executive;
import com.us.masterproject.model.Target;
import com.us.masterproject.service.AdminService;
import com.us.masterproject.serviceImpl.FileStorageServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.List;

import static com.us.masterproject.utils.ModelConstant.FILE_MUST;
import static com.us.masterproject.utils.ModelConstant.SUCCESS;

@Slf4j
@RestController
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private AdminService adminService;

    @Autowired
    private FileStorageServiceImpl fileStorageService;

    @PostMapping("/uploadFile")
    public ResponseEntity<?> uploadFileFromExcel(MultipartFile file, Integer executiveId) throws InvalidDataException {
        log.info("Enter uploadFileFromExcel() in AdminController with file name :{}", file.getOriginalFilename());
        try {
            if (!file.isEmpty()) {
                InputStream in = file.getInputStream();
                String responseMsg = adminService.uploadFileFromExcel(in, executiveId);
                if (responseMsg.endsWith(SUCCESS)) {
                    log.info("End uploadFileFromExcel() in AdminController with file name :{}", file.getOriginalFilename());
                    return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK).setMessage("fresh client added successfully")
                            .create();
                }
                log.info("End uploadFileFromExcel() in AdminController with file name :{}", file.getOriginalFilename());
                return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK).setMessage("fresh client not added")
                        .create();
            }
            throw new InvalidDataException(FILE_MUST);
        } catch (Exception e) {
            e.printStackTrace();
            throw new InvalidDataException(e.getMessage());
        }
    }

    @PostMapping("/addUpdateExecutive")
    public ResponseEntity<?> addUpdateExecutive(AddExecutiveReq addExecutiveReq) throws NotFoundException, InvalidDataException {
        log.info("Enter addUpdateExecutive() inside admin controller with request body: {}", addExecutiveReq);
        Executive executive = adminService.addUpdateExecutive(addExecutiveReq);
        if (executive.getMessage().endsWith(SUCCESS)) {
            log.info("Enter addUpdateExecutive() inside admin controller with msg: {}", executive.getMessage());
            return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK).setMessage("client added successfully")
                    .create();
        }
        log.info("End addExecutive() inside admin controller with msg: {}", executive.getMessage());
        return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK).setMessage(executive.getMessage())
                .create();
    }


    @GetMapping("/getAllExecutive")
    public ResponseEntity<?> getAllExecutive() {
        List<Executive> executiveResponse = adminService.getAllExecutive();
        if (CollectionUtils.isEmpty(executiveResponse)) {
            return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                    .setMessage("executive not found").setData(executiveResponse).create();
        }
        return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                .setMessage("All executive fetched successfully").setData(executiveResponse).create();
    }

    @GetMapping("/getExecutiveById/{executiveId}")
    public ResponseEntity<?> getExecutiveById(Integer executiveId) throws NotFoundException {
        ExecutiveResponse response = adminService.getExecutiveById(executiveId);
        return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                .setMessage("executive fetched successfully").setData(response).create();
    }

    @GetMapping("/getAllExecutiveByAdminId/{adminId}")
    public ResponseEntity<?> getAllExecutiveByAdminId(Integer adminId) throws NotFoundException {
        List<ExecutiveResponse> executiveResponse = adminService.getAllExecutiveByAdminId(adminId);
        if (CollectionUtils.isEmpty(executiveResponse)) {
            return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                    .setMessage("executive not found").setData(executiveResponse).create();
        }
        return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                .setMessage("All executive fetched successfully").setData(executiveResponse).create();
    }

    @PutMapping("/deleteExecutiveById/{executiveId}")
    public ResponseEntity<?> deleteExecutiveById(Integer executiveId) throws NotFoundException {
        adminService.deleteExecutiveById(executiveId);
        return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                .setMessage("executive fetched successfully").create();
    }

    @PostMapping("/addUpdateTarget")
    public ResponseEntity<?> addUpdateTarget(AddTargetReq addTargetReq) throws NotFoundException, InvalidDataException {
        log.info("Enter addUpdateTarget() inside executive controller with request body: {}", addTargetReq);
        Target target = adminService.addUpdateTarget(addTargetReq);
        if (target.getMessage().endsWith(SUCCESS)) {
            log.info("Enter addUpdateTarget() inside executive controller with msg: {}", target.getMessage());
            return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK).setMessage("Submit target Form Successfully")
                    .create();
        }
        log.info("End addUpdateTarget() inside executive controller with msg: {}", target.getMessage());
        return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK).setMessage(target.getMessage())
                .create();
    }

    @GetMapping("/getAllTargetByExecutiveId/{executiveId}")
    public ResponseEntity<?> getAllTargetByExecutiveId(Integer executiveId) throws NotFoundException {
        List<TargetResponse> targetResponses = adminService.getAllTargetByExecutiveId(executiveId);
        if (CollectionUtils.isEmpty(targetResponses)) {
            return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                    .setMessage("target details not found").setData(targetResponses).create();
        }
        return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                .setMessage("All target  details fetched successfully").setData(targetResponses).create();
    }

    @GetMapping("/getTargetByTargetId/{targetId}")
    public ResponseEntity<?> getTargetByTargetId(Integer targetId) throws NotFoundException {
        TargetResponse targetResponses = adminService.getTargetByTargetId(targetId);
        if (ObjectUtils.isEmpty(targetResponses)) {
            return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                    .setMessage("target details not found").setData(targetResponses).create();
        }
        return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                .setMessage("target  details fetched successfully").setData(targetResponses).create();
    }


    @GetMapping("/getAllAttendanceByExecutiveId/{executiveId}")
    public ResponseEntity<?> getAllAttendanceByExecutiveId(@RequestParam("executiveId") Integer executiveId, @RequestParam("month") Integer month, @RequestParam("year") Integer year) throws NotFoundException, InvalidDataException {
        List<AttendanceResponse> attendanceResponse = adminService.getAllAttendanceByExecutiveId(executiveId, month, year);
        if (CollectionUtils.isEmpty(attendanceResponse)) {
            return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                    .setMessage("Attendance not found").setData(attendanceResponse).create();
        }
        return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                .setMessage("All attendance details fetched successfully").setData(attendanceResponse).create();
    }

    @PostMapping("/addUpdateExcelSheetUrl")
    public ResponseEntity<?> addUpdateExcelSheetUrl(ExcelSheetUrlDto excelSheetUrlDto) throws NotFoundException, InvalidDataException {
        log.info("Enter addUpdateExcelSheetUrl() inside admin controller with request body: {}", excelSheetUrlDto);
        ExcelSheetUrl excelSheetUrl = adminService.addUpdateExcelSheetUrl(excelSheetUrlDto);
        if (excelSheetUrl.getMessage().endsWith(SUCCESS)) {
            log.info("Enter addUpdateExcelSheetUrl() inside admin controller with msg: {}", excelSheetUrl.getMessage());
            return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK).setMessage(excelSheetUrl.getMessage())
                    .create();
        }
        log.info("End addUpdateExcelSheetUrl() inside admin controller with msg: {}", excelSheetUrl.getMessage());
        return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK).setMessage(excelSheetUrl.getMessage())
                .create();
    }

    @GetMapping("/getAllExcelSheetUrlByAdminId/{adminId}")
    public ResponseEntity<?> getAllExcelSheetUrlByAdminId(@RequestParam("adminId") Integer adminId) throws NotFoundException, InvalidDataException {
        List<ExcelSheetUrlDto> excelSheetUrlDtos = adminService.getAllExcelSheetUrlByAdminId(adminId);
        if (CollectionUtils.isEmpty(excelSheetUrlDtos)) {
            return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                    .setMessage("excelSheet url not found").setData(excelSheetUrlDtos).create();
        }
        return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                .setMessage("All excelSheet url fetched successfully").setData(excelSheetUrlDtos).create();
    }

    @DeleteMapping("/deleteExcelSheetUrlById/{excelSheetUrlId}")
    public ResponseEntity<?> deleteExcelSheetUrlById(Integer excelSheetUrlId) throws NotFoundException {
        adminService.deleteExcelSheetUrlById(excelSheetUrlId);
        return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK).setErrorStatus(HttpStatus.OK)
                .setMessage("excelSheet url delete successfully").create();
    }
}
