package com.us.masterproject.exception;

public class InvalidDataException extends Exception {

	private static final long serialVersionUID = 2181174869331082282L;

	public InvalidDataException(String message) {
		super(message);
	}
}