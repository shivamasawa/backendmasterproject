package com.us.masterproject.exception;

import com.us.masterproject.handler.GenericResponseHandler;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
class GlobalControllerExceptionHandler {

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler({ InvalidDataException.class })
	public ResponseEntity<?> handleInvalidDataException(Exception ex) {
		return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK)
				.setData(null).setErrorStatus(HttpStatus.BAD_REQUEST).setMessage(ex.getMessage()).create();
	}
	
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler({ NotFoundException.class })
	public ResponseEntity<?> handleNotFoundRequest(Exception ex) {
	   return new GenericResponseHandler.Builder().setStatus(HttpStatus.OK)
			   .setData(null).setErrorStatus(HttpStatus.NOT_FOUND).setMessage(ex.getMessage()).create();
	}
}